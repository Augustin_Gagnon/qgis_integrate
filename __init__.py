# -*- coding: utf-8 -*-

def classFactory(iface):
    """ Load the IntegratePlugin class from plugin file """

    from QGIS_Integrate.plugin import IntegratePlugin
    return IntegratePlugin(iface)
