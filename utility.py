# -*- coding: utf-8 -*-

import os
import warnings
import base64
import collections

from PyQt4 import QtGui
from PyQt4 import QtCore

from qgis.PyQt.uic import loadUiType

warnings.filterwarnings("ignore")

class StaticPath(object):
    """
    Utility object to get the absolute path to the plugin directory. It need to be a class to use the __file__ keyword
    """
    def __init__(self):
        self.path = os.path.dirname(os.path.abspath(__file__))



def loadPackage(package_name):
    """
    Load Python package if it does not exist. The package pip is required to install other package
    """
    try:
        import pip
    except:
        path = '%s%s%s' % (os.path.dirname(os.path.abspath(__file__)), os.sep, 'get-pip.py')
        execfile(path)

    print('installing package: '+package_name)
    pip.main(['install', '--upgrade', package_name])


def loadUiClass(ui_file):
    """
    Return the class object corresponding to ui_file
    """
    ui_file_full = '%s%sui%s%s' % (os.path.dirname(os.path.abspath(__file__)), os.sep, os.sep, ui_file)
    return loadUiType(ui_file_full)[0]


def dictToString(data):
    """
    Convert all the keys and values of a dictionnary to ascii
    In Python 2, default dictionnary encoding is unicode but most function can not work with
    """
    if isinstance(data, basestring):
        return (data).encode('ascii', 'ignore')
    elif isinstance(data, collections.Mapping):
        return dict(map(dictToString, data.iteritems()))
    elif isinstance(data, collections.Iterable):
        return type(data)(map(dictToString, data))
    else:
        return data


class QTaskWidget(QtGui.QWidget):
    """
    Custom Widget class to show the tasks of the session, in the QListWidget
    """
    def __init__ (self):
        super(QTaskWidget, self).__init__()
        self.setupUi()

        self.plugin_path = StaticPath().path
        self.status = "NOT_STARTED"

        # create all required image
        self.icon_not_started = QtGui.QPixmap('%s%simg%s%s' % (self.plugin_path, os.sep, os.sep, 'icon_open.png'))
        self.icon_running = QtGui.QPixmap('%s%simg%s%s' % (self.plugin_path, os.sep, os.sep, 'icon_progress.png'))
        self.icon_finished = QtGui.QPixmap('%s%simg%s%s' % (self.plugin_path, os.sep, os.sep, 'icon_complete.png'))
        self.icon_paused = QtGui.QPixmap('%s%simg%s%s' % (self.plugin_path, os.sep, os.sep, 'icon_pending.png'))

        self.label_icon.setPixmap(self.icon_not_started)


    def setupUi(self):
        """
        Constructor for the widget appearance
        """
        # the upper part of the widget
        self.top_layout = QtGui.QHBoxLayout()
        self.top_layout.setSpacing(0)

        self.label_kind = QtGui.QLabel()
        self.label_kind.setFixedWidth(100)
        self.label_kind.setStyleSheet("font-weight: bold;")

        self.progress_bar = QtGui.QProgressBar()
        self.progress_bar.setFixedWidth(150)
        self.progress_bar.setAlignment(QtCore.Qt.AlignCenter)
        self.progress_bar.setVisible(False)
        self.progress_bar.reset()

        self.top_layout.addWidget(self.label_kind)
        self.top_layout.addItem(QtGui.QSpacerItem(0, 0, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum))
        self.top_layout.addWidget(self.progress_bar)

        # the bottom part of the widget
        self.bottom_layout = QtGui.QHBoxLayout()

        self.label_detail = QtGui.QLabel()
        self.label_conformance = QtGui.QLabel()
        self.label_conformance.setAlignment(QtCore.Qt.AlignCenter)
        self.label_conformance.setVisible(False)

        self.bottom_layout.addWidget(self.label_detail)
        self.bottom_layout.addWidget(self.label_conformance)

        # the general layout
        self.right_layout = QtGui.QVBoxLayout()
        self.right_layout.addLayout(self.top_layout)
        self.right_layout.addLayout(self.bottom_layout)

        self.global_layout = QtGui.QHBoxLayout()
        self.global_layout.setSpacing(0)

        self.label_icon = QtGui.QLabel()
        self.label_icon.setFixedWidth(30)
        self.global_layout.addWidget(self.label_icon)
        self.global_layout.addLayout(self.right_layout)
        self.setLayout(self.global_layout)



    def setKindText(self, kind_text):
        """
        Set the text of the first QLabel
        """
        self.kind = kind_text
        string = "<b>"
        for i in range(len(kind_text)):     # go through every character
            if(kind_text[i].isupper() and i != 0):     # if it's an upper case, add a space before it
                string += " " + kind_text[i]
            else:
                string += kind_text[i]
        string += "</b>"
        self.label_kind.setText(string)


    def setDetailText(self, detail_text):
        """
        Set the text of the second QLabel
        """
        string = detail_text
        if(detail_text != ""):
            # string = detail_text[1:]  # remove first '/'
            array = string.split("/")   # split the string in a list
            string = " / ".join(array[1:])    # recreate the string without the first array element ('rules', 'actions', etc.)
        else:
            string = ""
        self.label_detail.setText(string)


    def updateValues(self, total, processed, count):
        if(self.status == "RUNNING" and (self.kind == "CheckRules" or self.kind == "ApplyActions")):
            if(total != 0):
                value = 100 * processed / total
                self.progress_bar.setValue(value)
                self.progress_bar.setVisible(True)
        else:
            self.progress_bar.setVisible(False)

        if(self.status == "FINISHED" and self.kind == "CheckRules"):
            if(total != 0):
                percent = 100 * (total-count) / total
                string = "Conformance: %0.2f%%" % (percent)
                self.label_conformance.setText(string)
                self.label_conformance.setVisible(True)
        else:
            self.label_conformance.setVisible(False)


    def setTaskStatus(self, state):
        """
        Check the status of an item and change the style
        """
        self.status = state
        if(state == "NOT_STARTED"):
            self.label_kind.setStyleSheet("color: rgb(0,0,0);")
            self.label_icon.setPixmap(self.icon_not_started)

        elif(state == "RUNNING"):
            self.label_kind.setStyleSheet("color: rgb(0,176,255);")
            self.label_icon.setPixmap(self.icon_running)

        elif(state == "PAUSED"):
            self.label_kind.setStyleSheet("color: rgb(50,50,50);")
            self.label_icon.setPixmap(self.icon_paused)

        elif(state == "FINISHED"):
            self.label_kind.setStyleSheet("color: rgb(0,200,83);")
            self.label_icon.setPixmap(self.icon_finished)
