# -*- coding: utf-8 -*-

from PyQt4 import QtCore
from PyQt4 import QtGui
from qgis.core import *

import os
from QGIS_Integrate.utility import loadPackage, StaticPath

from QGIS_Integrate.dialogs.connection_dialog import ConnectionDialog
from QGIS_Integrate.dialogs.main_dialog import MainDialog

class IntegratePlugin:

    def __init__(self, iface):
        """Constructor"""
        self.iface = iface
        self.plugin_path = StaticPath().path
        self.action = None
        self.plugin_menu = '&1Spatial'

        self.connect_dialog = None
        self.main_dialog = None

        self.session = None
        self.url = None


    def initGui(self):
        """
        Create the menu item and toolbar icon inside the QGIS UI
        """
        # set the plugin icon, add it to QGIS, and connect it to the plugin
        action_icon = QtGui.QIcon('%s%s%s' % (self.plugin_path, os.sep, 'img/favicon.png'))
        self.action = QtGui.QAction(action_icon, "1Integrate", self.iface.mainWindow())
        self.action.triggered.connect(self.run)

        # add toolbar button and menu item
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu(self.plugin_menu, self.action)

        self.connect_dialog = ConnectionDialog(self.iface)


    def unload(self):
        """
        Removes the plugin menu item and icon from QGIS UI
        """
        self.iface.removePluginMenu(self.plugin_menu, self.action)
        self.iface.removeToolBarIcon(self.action)


    def run(self):
        if(self.main_dialog != None and self.main_dialog.isVisible()):
            return

        if(self.session == None):       # if the user isn't connected yet
            success = self.connectUser()    # start thr connection widget
            if(success):
                self.main_dialog = MainDialog(self.iface, self.session, self.url)
                QtCore.QObject.connect(self.main_dialog, QtCore.SIGNAL("disconnect_user"), self.disconnectUser)


        if(self.session != None):
            self.main_dialog.show()


    def connectUser(self):
        self.session = None
        self.connect_dialog.show()

        result = self.connect_dialog.exec_()
        if result:
            self.session = self.connect_dialog.session
            self.url = self.connect_dialog.url

        return result

    def disconnectUser(self):
        self.session = None
