# -*- coding: utf-8 -*-

import os

from PyQt4 import QtGui
from PyQt4 import QtCore
from qgis.core import *
# from qgis.gui import QgsMessageBar

from QGIS_Integrate.utility import StaticPath, loadUiClass, dictToString


FORM_CLASS = loadUiClass('editor_dialog.ui')

class EditorDialog(QtGui.QDialog, FORM_CLASS):
        """
        Custom QDialog to edit a rule
        """
        def __init__ (self,json_content,difference_list,available_attributes):
            super(EditorDialog, self).__init__()
            self.setupUi(self)

            self.plugin_path = StaticPath().path
            self.setWindowIcon( QtGui.QIcon('%s%simg%s%s' % (self.plugin_path, os.sep, os.sep, 'favicon.png')) )
            self.setWindowTitle("Rule Editor")

            self.json_content = json_content
            self.difference_list = difference_list
            self.available_attributes = available_attributes

            self.replacement_list = []
            self.combo_box_list = []

            self.connect(self, QtCore.SIGNAL("showFinished()"), self.initWidget)
            self.button_accept.clicked.connect(self.acceptDialog)
            self.button_cancel.clicked.connect(self.cancelDialog)


        def show(self):
            super(EditorDialog, self).show()
            self.emit( QtCore.SIGNAL('showFinished()') )    # emit signal to initialize the widget logic


        def initWidget(self):
            self.setContentDefinition()
            self.setModificationLayout()


        def acceptDialog(self):
            self.setReplacement()
            self.accept()

        def cancelDialog(self):
            self.reject()


        def setContentDefinition(self):
            root_predicate = self.json_content['rootTerm']['predicates'][0]
            text = self.predicateToText(root_predicate)

            tree_item = QtGui.QTreeWidgetItem()
            tree_item.setText(0, text)
            self.content_definition_tree.addTopLevelItem(tree_item)

            queue = []
            for predicate in root_predicate['predicates']:
                queue.append({'predicate': predicate, 'parent': tree_item})

            while(len(queue) > 0):
                item = queue.pop(0)
                predicate = item['predicate']
                parent_item = item['parent']

                text = self.predicateToText(predicate)

                tree_item = QtGui.QTreeWidgetItem()
                tree_item.setText(0, text)
                parent_item.addChild(tree_item)

                if(predicate.get('predicates')):
                    for p in predicate['predicates']:
                        queue.append({'predicate': p, 'parent': tree_item})

            self.content_definition_tree.expandAll()


        def setModificationLayout(self):
            self.replacement_list = []
            self.combo_box_list = []

            for class_name in self.difference_list['class']:
                text = "Select a class to replace: <b>" + class_name + "</b>"
                item_list = self.available_attributes.keys()

                self.replacement_list.append({'old': class_name, 'new': "", 'type': "class"})
                self.addLayoutWidget(text, item_list)

            for prop in self.difference_list['property']['missing_class']:
                prop_name = prop.split(':')[0]
                class_name = prop.split(':')[1]
                text = "Select a property to replace: <b>" + prop_name + "</b> from missing class: " + class_name

                item_list = []
                for key in self.available_attributes.keys():
                    for attr in self.available_attributes[key]:
                        item = str(key) + "." + str(attr)
                        item_list.append(item)

                self.replacement_list.append({'old': prop_name, 'new': "", 'type': "property"})
                self.addLayoutWidget(text, item_list)

            for prop in self.difference_list['property']['existing_class']:
                prop_name = prop.split(':')[0]
                class_name = prop.split(':')[1]
                text = "Select a property to replace: <b>" + prop_name + "</b> in: " + class_name

                item_list = []
                for attr in self.available_attributes[class_name]:
                    item = str(class_name) + "." + str(attr)
                    item_list.append(item)

                self.replacement_list.append({'old': prop_name, 'new': "", 'type': "property"})
                self.addLayoutWidget(text, item_list)


        def addLayoutWidget(self, text, item_list):
            label = QtGui.QLabel(text, self)
            combo_box = QtGui.QComboBox(self)
            combo_box.addItems(item_list)
            combo_box.setCurrentIndex(-1)       # set no default value for the combo_box
            self.combo_box_list.append(combo_box)

            h_layout = QtGui.QHBoxLayout(self)
            h_layout.addWidget(label)
            h_layout.addWidget(combo_box)
            self.vertical_layout.addLayout(h_layout)


        def setReplacement(self):
            for i in range(len(self.combo_box_list)):
                text = self.combo_box_list[i].currentText()

                if(text != ""):
                    difference_type = self.replacement_list[i]['type']
                    if(difference_type == "class"):
                        attribute_name = text
                    else:
                        attribute_name = text.split(".")[1]

                    self.replacement_list[i]['new'] = attribute_name
                else:
                    self.replacement_list[i]['new'] = self.replacement_list[i]['old']


        def getNewContent(self):
            new_content = dictToString(self.json_content)
            for i in range(len(self.replacement_list)):
                old_value = str(self.replacement_list[i]['old'])
                new_value = str(self.replacement_list[i]['new'])
                self.switchJsonValue(new_content,old_value,new_value)
            print(new_content)
            return new_content


        def switchJsonValue(self,json,previous_value,new_value):
            """
            Recursive method to change a specific value to a new one inside a json
            """
            for key in json.keys():
                if json[key] == previous_value:
                    json[key] = new_value
                elif type(json[key]) is dict:
                    self.switchJsonValue(json[key], previous_value, new_value)
                elif type(json[key]) is list:
                    for item in json[key]:
                        self.switchJsonValue(item, previous_value, new_value)


        def predicateToText(self,predicate):
            kind = predicate['kind']
            text = ""

            if(kind == "RootPredicate"):
                text = "Rule for"
                if(predicate['attributes']['classLabel'] != ""):
                    text += " " + str(predicate['attributes']['classLabel'])
                text += " objects"
                if(predicate['attributes']['objLabel'] != ""):
                    text += " " + str(predicate['attributes']['objLabel'])
                text += ":"

            elif(kind == "RootOperation"):
                text = "Action for"
                if(predicate['attributes']['classLabel'] != ""):
                    text += " " + str(predicate['attributes']['classLabel'])
                text += " objects"
                if(predicate['attributes']['objLabel'] != ""):
                    text += " " + str(predicate['attributes']['objLabel'])
                text += ":"


            ### Condition ###
            elif(kind == "RelationalPredicate"):
                text = "Relation:"        # value1 relation value2

            elif(kind == "ReferentialPredicate"):
                text = "Referenced"
                if(predicate['attributes']['classRef1'] != ""):
                    text += " from " + str(predicate['attributes']['classRef1'])
                if(predicate['attributes']['objRef1'] != ""):
                    text += "." + str(predicate['attributes']['objRef1'])
                if(predicate['attributes']['classRef2'] != ""):
                    text += " to " + str(predicate['attributes']['classRef2'])
                if(predicate['attributes']['objRef2'] != ""):
                    text += "." + str(predicate['attributes']['objRef2'])
                if(predicate['attributes']['refName'] != ""):
                    text += " as " + str(predicate['attributes']['refName'])

            elif(kind == "RangePredicate"):
                text = "Test in range:"     # value1 'is in the range' [value2 , value3]

            elif(kind == "AndPredicate"):
                text = "Logical AND"

            elif(kind == "OrPredicate"):
                text = "Logical OR"

            elif(kind == "XorPredicate"):
                text = "Logical XOR"

            elif(kind == "NotPredicate"):
                text = "Logical NOT"

            elif(kind == "ConditionalPredicate"):
                text = "If... Then..."

            elif(kind == "ExistsPredicate"):
                text = "It exists"
                if(predicate['attributes']['qualifier'] != ""):
                    text += " " + str(predicate['attributes']['qualifier'])
                if(predicate['attributes']['n'] != ""):
                    text += " " + str(predicate['attributes']['n'])
                if(predicate['attributes']['classLabel'] != ""):
                    text += " " + str(predicate['attributes']['classLabel']) + " object(s)"
                else:
                    text += " object(s)"
                if(predicate['attributes']['objLabel'] != ""):
                    text += " " + str(predicate['attributes']['objLabel'])
                text += " for which..."

            elif(kind == "ForAllPredicate"):
                text = "For all"
                if(predicate['attributes']['classLabel'] != ""):
                    text += " " + str(predicate['attributes']['classLabel']) + " object(s)"
                else:
                    text+= " object(s)"
                if(predicate['attributes']['objLabel'] != ""):
                    text += " " + str(predicate['attributes']['objLabel'])
                text += " for which... check that..."

            elif(kind == "ChainPredicate"):
                text = "Chain from"
                if(predicate['attributes']['classRef'] != ""):
                    text += " " + str(predicate['attributes']['classRef'])
                text += " connected by..."

            elif(kind == "ElementExistsPredicate"):
                text = "It exists"
                if(predicate['attributes']['qualifier'] != ""):
                    text += " " + str(predicate['attributes']['qualifier'])
                if(predicate['attributes']['n'] != ""):
                    text += " " + str(predicate['attributes']['n'])
                if(predicate['attributes']['classLabel'] != ""):
                    text += " " + str(predicate['attributes']['classLabel']) + " object(s)"
                else:
                    text += " object(s)"
                if(predicate['attributes']['objLabel'] != ""):
                    text += " " + str(predicate['attributes']['objLabel'])

                text += " in..."        # in value1
                text += " for which..."

            elif(kind == "ForAllElementsPredicate"):
                text = "For all"
                if(predicate['attributes']['classLabel'] != ""):
                    text += " " + str(predicate['attributes']['classLabel']) + " object(s)"
                else:
                    text+= " object(s)"
                if(predicate['attributes']['objLabel'] != ""):
                    text += " " + str(predicate['attributes']['objLabel'])
                text += " in..."        # in value1
                text += " check that..."


            ### Operation ###
            elif(kind == "AssignmentOperation"):
                text = "Let ... = ..."

            elif(kind == "BuiltinOperation"):
                text = "Apply"
                if(predicate['attributes']['fnName'] != ""):
                    text += " " + str(predicate['attributes']['fnName']) + "(...)"

            elif(kind == "ConditionalOperation"):
                text = "If... Then..."

            elif(kind == "SequenceOperation"):
                text = "Sequence:"

            elif(kind == "WhileLoopOperation"):
                text = "While... Loop"

            elif(kind == "ForPredicateOperation"):
                text = "For"
                if(predicate['attributes']['n'] == "" or predicate['attributes']['n'] == "0"):
                    text += " all"
                else:
                    text += " " + predicate['attributes']['n']
                if(predicate['attributes']['classLabel'] != ""):
                    text += " " + str(predicate['attributes']['classLabel'])
                text+= " object(s)"
                if(predicate['attributes']['objLabel'] != ""):
                    text += " " + str(predicate['attributes']['objLabel'])
                text += " for which..."

            elif(kind == "ForAllElementsOperation"):
                text = "For all"
                if(predicate['attributes']['classLabel'] != ""):
                    text += " " + predicate['attributes']['classLabel']
                text += " elements"
                if(predicate['attributes']['objLabel'] != ""):
                    text += " " + predicate['attributes']['objLabel']
                text += " in... do..."

            elif(kind == "BreakOperation"):
                text = "Break"

            elif(kind == "ReportOperation"):
                text = "Report..."

            elif(kind == "CreateObjectOperation"):
                text = "Create an object named"
                if(predicate['attributes']['objLabel'] != ""):
                    text += " " + predicate['attributes']['objLabel']
                else:
                    text += " ..."
                text += " of class"
                if(predicate['attributes']['classLabel'] != ""):
                    text += " " + str(predicate['attributes']['classLabel'])
                else:
                    text += " ..."
                text += ":"

            elif(kind == "DeleteObjectOperation"):
                text = "Delete object"
                if(predicate['attributes']['classRef'] != ""):
                    text += " " + str(predicate['attributes']['classRef'])
                if(predicate['attributes']['objRef'] != ""):
                    text += ":" + str(predicate['attributes']['objRef'])


            ### Value ###
            elif(kind == "NullValue"):
                text = "null"

            elif(kind == "StaticValue"):
                text = ""
                if(predicate['attributes']['value'] != ""):
                    text += str(predicate['attributes']['value'])

            elif(kind == "DynamicValue"):
                text = ""
                if(predicate['attributes']['classRef'] != ""):
                    text += str(predicate['attributes']['classRef'])
                if(predicate['attributes']['objRef'] != ""):
                    text += ":" + str(predicate['attributes']['objRef'])
                if(predicate['attributes']['propName'] != ""):
                    text += "." + str(predicate['attributes']['propName'])

            elif(kind == "ConstantValue"):
                text = "Constant"
                if(predicate['attributes']['classLabel'] != ""):
                    text += " " + str(predicate['attributes']['classLabel'])
                if(predicate['attributes']['key'] != ""):
                    text += "/" + str(predicate['attributes']['key'])

            elif(kind == "ObjectValue"):
                text = ""
                if(predicate['attributes']['classRef'] != ""):
                    text += str(predicate['attributes']['classRef'])
                if(predicate['attributes']['objRef'] != ""):
                    text += ":" + str(predicate['attributes']['objRef'])

            elif(kind == "ClassValue"):
                text = "class("
                if(predicate['attributes']['classRef'] != ""):
                    text += str(predicate['attributes']['classRef'])
                if(predicate['attributes']['objRef'] != ""):
                    text += ":" + str(predicate['attributes']['objRef'])
                text += ")"

            elif(kind == "NestedPropertyValue"):
                text = "Nested Property:"      # value1 . value2

            elif(kind == "ValuePath"):      # goes with the NestedPropertyValue
                text = ""
                if(predicate['attributes']['propName'] != ""):
                    text += str(predicate['attributes']['propName'])

            elif(kind == "ArrayElementValue"):
                text = "Array from... to..."     # [value1, value2]

            elif(kind == "BuiltinFnValue"):
                text = ""
                if(predicate['attributes']['fnName'] != ""):
                    text += str(predicate['attributes']['fnName'])
                text += "(...)"     # (value)

            elif(kind == "AggreggateValue"):
                text = ""
                if(predicate['attributes']['fnName'] != ""):
                    text = str(predicate['attributes']['fnName']) + "(...)"
                text += " over all "
                if(predicate['attributes']['classLabel'] != ""):
                    text += str(predicate['attributes']['classLabel'])
                if(predicate['attributes']['objLabel'] != ""):
                    text += ":" + str(predicate['attributes']['objLabel'])
                text += " for which..."

            elif(kind == "NegatedValue"):
                text = "Minus:"     # 'Minus: ' -( value )

            elif(kind == "SummedValue"):
                text = "Sum:"      # 'Sum: ' value1 + value2

            elif(kind == "DifferenceValue"):
                text = "Difference:"    # 'Difference: ' value1 - value2

            elif(kind == "ProductValue"):
                text = "Product:"    # 'Product: ' value1 x value2

            elif(kind == "DivisionValue"):
                text = "Divide:"    # 'Divide: ' value1 / value2

            elif(kind == "ModulusValue"):
                text = "Remainder:"    # 'Remainder: ' value1 % value2

            elif(kind == "ConditionalValue"):
                text = "Conditional:"    # 'Conditional: ' condition ? value1 : value2


            ### Relationship ###
            elif(kind == "EqualsRelation"):
                text = "equals"

            elif(kind == "NotEqualsRelation"):
                text = "does not equal"

            elif(kind == "LessRelation"):
                text = "is less than"

            elif(kind == "LessEqualsRelation"):
                text = "is less than or equal to"

            elif(kind == "GreaterRelation"):
                text = "is greater than"

            elif(kind == "GreaterEqualsRelation"):
                text = "is greater than or equal to"

            elif(kind == "BeginsRelation"):
                text = "begins with"

            elif(kind == "EndsRelation"):
                text = "ends with"

            elif(kind == "ContainsRelation"):
                text = "contains"

            elif(kind == "RegExpRelation"):
                text = "matches"

            elif(kind == "SpatialEqualsRelation"):
                text = "equals"

            elif(kind == "SpatialDisjointRelation"):
                text = "is disjoint from"

            elif(kind == "SpatialIntersectsRelation"):
                text = "intersects"

            elif(kind == "SpatialTouchesRelation"):
                text = "touches"

            elif(kind == "SpatialOverlapsRelation"):
                text = "overlaps"

            elif(kind == "SpatialCrossesRelation"):
                text = "crosses"

            elif(kind == "SpatialWithinRelation"):
                text = "is contained within"

            elif(kind == "SpatialContainsRelation"):
                text = "contains"

            elif(kind == "SpatialWithinDistRelation"):
                text = "is with a distance of"
                if(predicate['attributes']['distance'] != ""):
                    text += " " + str(predicate['attributes']['distance'])
                text += " of"

            elif(kind == "SpatialBeyondRelation"):
                text = "is farther than"
                if(predicate['attributes']['distance'] != ""):
                    text += " " + str(predicate['attributes']['distance'])
                text += " from"

            elif(kind == "SpatialCoversRelation"):
                text = "covers"

            elif(kind == "SpatialCoveredByRelation"):
                text = "covered by"

            else:
                text = "<<" + kind + ">>"

            return text
