# -*- coding: utf-8 -*-

import os

from PyQt4 import QtGui
from PyQt4 import QtCore
from qgis.core import *

class ListInputDialog(QtGui.QDialog):
    def __init__(self, input_list):
        super(ListInputDialog, self).__init__()
        self.setupUi()
        self.setWindowTitle("List Selection")

        self.output_list = []
        self.list_widget.addItems(input_list)

        self.button_cancel.clicked.connect(self.cancelDialog)
        self.button_accept.clicked.connect(self.acceptDialog)


    def setupUi(self):
        self.hint_label = QtGui.QLabel("Select item(s):")

        self.list_widget = QtGui.QListWidget()
        self.list_widget.setSelectionMode(2)        # allow multi-selection

        self.button_cancel = QtGui.QPushButton("Cancel")
        self.button_accept = QtGui.QPushButton("Ok")

        h_layout = QtGui.QHBoxLayout()
        h_layout.addItem(QtGui.QSpacerItem(0, 0, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum))
        h_layout.addWidget(self.button_accept)
        h_layout.addWidget(self.button_cancel)

        v_layout = QtGui.QVBoxLayout()
        v_layout.addWidget(self.hint_label)
        v_layout.addWidget(self.list_widget)
        v_layout.addLayout(h_layout)

        self.setLayout(v_layout)


    def acceptDialog(self):
        selected_items = self.list_widget.selectedItems()
        for item in selected_items:
            self.output_list.append(item.text())

        self.accept()

    def cancelDialog(self):
        self.reject()

    def getResult(self):
        return self.output_list



class TreeInputDialog(QtGui.QDialog):
    def __init__(self, list_input):
        super(TreeInputDialog, self).__init__()
        self.setupUi()
        self.setWindowTitle("Tree Selection")

        self.output_list = []
        self.createTree(list_input)

        self.button_cancel.clicked.connect(self.cancelDialog)
        self.button_accept.clicked.connect(self.acceptDialog)

        self.tree_widget.itemClicked.connect(self.handleSelection)

    def setupUi(self):
        self.hint_label = QtGui.QLabel("Select item(s):")

        self.tree_widget = QtGui.QTreeWidget()
        self.tree_widget.setSelectionMode(2)        # allow multi-selection
        self.tree_widget.header().hide()

        self.button_cancel = QtGui.QPushButton("Cancel")
        self.button_accept = QtGui.QPushButton("Ok")

        h_layout = QtGui.QHBoxLayout()
        h_layout.addItem(QtGui.QSpacerItem(0, 0, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum))
        h_layout.addWidget(self.button_accept)
        h_layout.addWidget(self.button_cancel)

        v_layout = QtGui.QVBoxLayout()
        v_layout.addWidget(self.hint_label)
        v_layout.addWidget(self.tree_widget)
        v_layout.addLayout(h_layout)

        self.setLayout(v_layout)


    def createTree(self, input_list):
        queue = input_list[:]
        while(len(queue) > 0):
            obj = queue.pop(0)
            node_name = obj['name']
            node_parent = obj['parent']

            tree_item = QtGui.QTreeWidgetItem()
            tree_item.setText(0, node_name)

            if(node_parent == "rules" or node_parent == "actions"):
                self.tree_widget.addTopLevelItem(tree_item)
            else:
                parent_item = self.findElementByName(node_parent)
                if(parent_item != None):
                    parent_item.addChild(tree_item)
                else:
                    queue.append(obj)


    def findElementByName(self, name):
        """
        Find an element in a QTreeWidget by name
        /!\ This should be changed to another method, as there can be two element with the same name
        """
        iterator = QtGui.QTreeWidgetItemIterator(self.tree_widget, QtGui.QTreeWidgetItemIterator.All)
        while iterator.value():
            item = iterator.value()
            if(item.text(0) == name):
                return item
            iterator +=1
        return None


    def handleSelection(self, tree_item):
        selected = tree_item.isSelected()
        self.toggleSelection(tree_item,selected)

    def toggleSelection(self, tree_item, parent_selection):
        child_count = tree_item.childCount()
        if(child_count > 0):
            for i in range(child_count):
                child = tree_item.child(i)
                child.setSelected(parent_selection)
                self.toggleSelection(child,parent_selection)



    def acceptDialog(self):
        selected_items = self.tree_widget.selectedItems()
        for item in selected_items:
            item_name = str(item.text(0))

            parent_item = item.parent()
            if(parent_item != None):
                parent_name = str(parent_item.text(0))
            else:
                parent_name = "top_level"

            self.output_list.append({'name': item_name, 'parent': parent_name})

        self.accept()

    def cancelDialog(self):
        self.reject()

    def getResult(self):
        return self.output_list
