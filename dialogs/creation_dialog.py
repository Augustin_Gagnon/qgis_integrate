# -*- coding: utf-8 -*-

import os
import requests
import zipfile
import urllib2

from PyQt4 import QtGui
from PyQt4 import QtCore
from qgis.core import *

from QGIS_Integrate.utility import StaticPath, loadUiClass, dictToString, QTaskWidget
from QGIS_Integrate.dialogs.input_dialog import ListInputDialog, TreeInputDialog
from QGIS_Integrate.dialogs.editor_dialog import EditorDialog

FORM_CLASS = loadUiClass('creation_dialog.ui')

class CreationDialog(QtGui.QDialog, FORM_CLASS):

    def __init__(self, iface, status_bar, session, url):
        """Constructor"""

        super(CreationDialog, self).__init__()
        self.setupUi(self)
        self.iface = iface
        self.status_bar = status_bar
        self.plugin_path = StaticPath().path

        self.folder_icon = QtGui.QIcon('%s%simg%s%s' % (self.plugin_path, os.sep, os.sep, 'icon_folder.ico'))

        self.session = session
        self.url = url
        self.info_thread = None
        self.update_thread = None
        self.thread_running = False

        self.session_datastores = []
        self.session_rules_and_actions = []
        self.session_running = False

        self.used_layers = []
        self.available_rules = []
        self.available_actions = []
        self.available_attributes = []

        self.editor_dialog = None
        self.list_dialog = None
        self.tree_dialog = None

        self.button_layers.clicked.connect(self.addLayers)
        self.button_rules.clicked.connect(self.addRules)
        self.button_actions.clicked.connect(self.addActions)
        self.button_delete.clicked.connect(self.deleteItem)
        self.button_start.clicked.connect(self.startSession)

        self.session_list.itemSelectionChanged.connect(self.toggleButton)


    def show(self):
        super(CreationDialog, self).show()

    def closeEvent(self, event):
        if(self.info_thread != None):
            self.info_thread.quit()
        if(self.update_thread != None):
            self.update_thread.quit()

        if(self.editor_dialog != None):
            self.editor_dialog.close()
        if(self.list_dialog != None):
            self.list_dialog.close()
        if(self.tree_dialog != None):
            self.tree_dialog.close()


    def initWidget(self):
        self.session_list.clear()
        self.available_rules = []
        self.available_actions = []

        self.info_thread = InfoThread(self.session, self.url)
        self.connect(self.info_thread, QtCore.SIGNAL("new_info(QString, PyQt_PyObject)"), self.handleInfo)
        self.connect(self.info_thread, QtCore.SIGNAL("finished()"), self.threadFinished)
        self.status_bar.showMessage("Loading ...")
        self.info_thread.start()
        self.thread_running = True

        self.resetSession()
        self.refreshSessionList()
        self.toggleButton()



    def threadFinished(self):
        self.status_bar.clearMessage()
        self.thread_running = False
        self.toggleButton()


    def handleInfo(self, arg, content):
        """
        Handle the QThread signal sending a new rule or action
        """
        node_category = arg.split("/")[0]
        node_name = arg.split("/")[-1]
        node_parent = arg.split("/")[-2]

        if(node_name == "Recycle Bin"):      # don't add the Recycle Bin
            return

        obj = {'name': node_name, 'parent': node_parent, 'url': arg, 'content': content}
        if(node_category == "rules"):
            self.available_rules.append(obj)
        else:
            self.available_actions.append(obj)


    def toggleButton(self):
        if(self.session_running):
            self.button_layers.setEnabled(False)
            self.button_rules.setEnabled(False)
            self.button_actions.setEnabled(False)
            self.button_delete.setEnabled(False)
            self.button_start.setEnabled(False)
        else:
            self.button_layers.setEnabled(True)

            if(self.thread_running):
                self.button_rules.setEnabled(False)
                self.button_actions.setEnabled(False)
            else:
                if(len(self.session_datastores) > 0):
                    self.button_rules.setEnabled(True)
                    self.button_actions.setEnabled(True)
                else:
                    self.button_rules.setEnabled(False)
                    self.button_actions.setEnabled(False)

            selection = self.session_list.selectedItems()
            if(len(selection) == 0):
                self.button_delete.setEnabled(False)
            else:
                self.button_delete.setEnabled(True)

            if(len(self.session_datastores) > 0 and len(self.session_rules_and_actions) > 0):
                self.button_start.setEnabled(True)
            else:
                self.button_start.setEnabled(False)


    def addLayers(self):
        # if(self.list_dialog != None):
        #     self.list_dialog.show()
        #     return

        available_layers = self.getAvailableLayers()
        self.list_dialog = ListInputDialog(available_layers)
        success = self.list_dialog.exec_()
        if(not success):
            return

        selected_layer_name = self.list_dialog.getResult()
        self.list_dialog = None

        selected_layer = self.getQgisLayer(selected_layer_name)
        for layer in selected_layer:
            layer_name = str(layer.name())
            text = "Uploading " + layer_name
            self.status_bar.showMessage(text)

            datastore_url = self.exportLayer(layer)
            if(datastore_url == "Error"):
                msg = QtGui.QMessageBox.warning(self, "Upload Error", "An error occured while trying to upload the following layer: "+layer_name)
                continue
            else:
                self.addToSession("datastore", datastore_url, layer_name)

            self.status_bar.clearMessage()


    def addRules(self):
        # if(self.tree_dialog != None):
        #     self.tree_dialog.show()
        #     return

        # show the dialog for the rule selection
        self.tree_dialog = TreeInputDialog(self.available_rules)
        success = self.tree_dialog.exec_()
        if(not success):
            return

        selected_rule_name = self.tree_dialog.getResult()
        self.tree_dialog = None

        # get the rule elements associated with the selected names (name, url, json)
        selected_rule = []
        for rule_element in selected_rule_name:
            rule = self.findElementByName(self.available_rules, rule_element['name'], rule_element['parent'])
            if(rule != None and rule['content'] != None):   # avoid keeping folder
                selected_rule.append(rule)

        for rule in selected_rule:
            rule_json = rule['content']

            # find attribute differences
            diff_list = self.findAttributeDifference(rule_json)
            print(diff_list)
            if(len(diff_list['class']) > 0 or len(diff_list['property']['missing_class']) > 0 or len(diff_list['property']['existing_class']) > 0 ):
                self.editor_dialog = EditorDialog(rule_json, diff_list, self.available_attributes)
                self.editor_dialog.show()

                success = self.editor_dialog.exec_()
                if(success):
                    new_rule_json = self.editor_dialog.getNewContent()
                else:
                    continue
            else:
                new_rule_json = rule_json

            # upload the new rule
            new_rule_json['version'] = None
            new_rule_name = str(rule['name'])
            new_rule_url = "rules/QGIS_Rules/" + new_rule_name.replace(" ", "_")

            exists = self.checkExists(new_rule_url)
            if(exists):
                print("Deleting previous rule")
                r = self.session.delete(self.url % new_rule_url, json={})
                if(r.status_code != requests.codes.ok):
                    print("Error while deleting previous rule")
                    print(r.text)
                    continue

            r = self.session.put(self.url % new_rule_url, json=new_rule_json)
            if r.status_code != requests.codes.ok:
                print("Error while uploading new edited rule")
                print(r.text)
                continue
            else:
                self.addToSession("rule", new_rule_url, new_rule_name)

    def addActions(self):
        # if(self.tree_dialog != None):
        #     self.tree_dialog.show()
        #     return

        # show the dialog for the action selection
        self.tree_dialog = TreeInputDialog(self.available_actions)
        success = self.tree_dialog.exec_()
        if(not success):
            return

        selected_action_name = self.tree_dialog.getResult()
        self.tree_dialog = None

        # get the action elements associated with the selected names (name, url, json)
        selected_action = []
        for action_element in selected_action_name:
            action = self.findElementByName(self.available_actions, action_element['name'], action_element['parent'])
            if(action != None and action['content'] != None):
                selected_action.append(action)

        for action in selected_action:
            action_json = action['content']

            # find attribute differences
            diff_list = self.findAttributeDifference(action_json)
            print(diff_list)
            if(len(diff_list['class']) > 0 or len(diff_list['property']['missing_class']) > 0 or len(diff_list['property']['existing_class']) > 0 ):
                self.editor_dialog = EditorDialog(action_json, diff_list, self.available_attributes)
                self.editor_dialog.show()

                success = self.editor_dialog.exec_()
                if(success):
                    new_action_json = self.editor_dialog.getNewContent()
                else:
                    continue
            else:
                new_action_json = action_json

            # upload the new action
            new_action_json['version'] = None
            new_action_name = str(action['name'])
            new_action_url = "rules/QGIS_Rules/" + new_action_name.replace(" ", "_")

            exists = self.checkExists(new_action_url)
            if(exists):
                r = self.session.delete(self.url % new_action_url, json={})
                if(r.status_code != requests.codes.ok):
                    print("Error while deleting previous rule")
                    print(r.text)
                    continue

            r = self.session.put(self.url % new_action_url, json=new_action_json)
            if r.status_code != requests.codes.ok:
                print("Error while uploading new edited action")
                continue
            else:
                self.addToSession("action", new_action_url, new_action_name)


    def deleteItem(self):
        idx = self.session_list.currentRow()
        print(idx)
        if(idx < len(self.session_datastores)):
            del self.session_datastores[idx]
        else:
            idx -= len(self.session_datastores)
            del self.session_rules_and_actions[idx]
        self.refreshSessionList()


    def addToSession(self, category, url, alias):
        if(category == "datastore"):
            self.session_datastores.append({'url': url, 'alias': alias, 'type': 'rule'})
            self.updateAvailableAttributes()
        elif(category == "rule"):
            self.session_rules_and_actions.append( {'url': url, 'alias': alias, 'type': 'rule'} )
        elif(category == "action"):
            self.session_rules_and_actions.append( {'url': url, 'alias': alias, 'type': 'action'} )
        self.refreshSessionList()


    def refreshSessionList(self):
        self.used_layers = []
        self.session_list.clear()
        for datastore in self.session_datastores:
            text = "Open Data: " + str(datastore['alias'])
            self.used_layers.append(str(datastore['alias']))
            self.session_list.addItem(text)

        for rule_and_action in self.session_rules_and_actions:
            if(rule_and_action['type'] == 'rule'):
                text = "Check Rule: " + str(rule_and_action['alias'])
            else:
                text = "Apply Action: " + str(rule_and_action['alias'])
            self.session_list.addItem(text)

        self.toggleButton()


    def startSession(self):
        session_url = "sessions/QGIS_Session/Auto_Session"

        # test if session already exist
        exists = self.checkExists(session_url)
        if(exists):
            result = QtGui.QMessageBox.warning(self, "Warning", "An automatic Session alreay exist.\nWould you like to replace it ?", QtGui.QMessageBox.Yes | QtGui.QMessageBox.No, QtGui.QMessageBox.Yes)
            print(result)
            if(result == QtGui.QMessageBox.Yes):
                self.session.delete(self.url % session_url, json={}).raise_for_status()
            else:
                return

        # upload the new session
        session_json = self.createSessionContent()
        r = self.session.put(self.url % session_url, json=session_json)
        if(r.status_code != requests.codes.ok):
            msg = QtGui.QMessageBox.critical(self, "Session Error", "An error occurend while trying to create the automatic session")
            return
        else:
            QtGui.QMessageBox.information(self, "Success", "Your automatic Session has been created\n The results will be displayed when the session is finished")

        # start the new sesssion
        self.session.put(self.url % session_url+'?action=play', json={}).raise_for_status()

        session_path = "/QGIS_Session/Auto_Session"
        self.update_thread = UpdateThread(self.session, self.url, session_path)
        self.connect(self.update_thread, QtCore.SIGNAL("new_update(PyQt_PyObject)"), self.trackSessionUpdate)
        self.update_thread.start()


    def resetSession(self):
        if(self.session_running):
            return

        session_url = "sessions/QGIS_Session/Auto_Session"
        exists = self.checkExists(session_url)
        if(exists):
            arg = session_url + "?action=stop"
            r = self.session.put(self.url % arg, json={})
            if(r.status_code == requests.codes.ok):
                r = self.session.delete(self.url % session_url, json={})
                if(r.status_code == requests.codes.ok):
                    print("session deleted successfully")
                else:
                    print(r.status_code)
                    print("Cannot delete session: "+session_url)
            else:
                print("Cannot stop session: "+session_url)


    def createSessionContent(self):
        json = {}
        json['description'] = "Sesssion created from QGIS"
        json['tasks'] = []

        for datastore in self.session_datastores:
            json['tasks'].append({ 'kind': 'OpenData', 'datastore': str('/'+datastore['url']) })

        for task in self.session_rules_and_actions:
            if(task['type'] == "rule"):
                json['tasks'].append({ 'kind': 'CheckRules', 'rules': [str('/'+task['url'])] })
            else:
                json['tasks'].append({ 'kind': 'ApplyActions', 'actions': [str('/'+task['url'])] })

        return json


    def trackSessionUpdate(self, json):
        if(json['status'] == "FINISHED"):
            self.update_thread.stop()
            self.update_thread.quit()

            self.status_bar.clearMessage()
            self.session_running = False
            self.handleSessionFinished(json)

        else:
            count = len(json['tasks'])
            current = 0
            for i in range(count):
                task = json['tasks'][i]
                if(task['status'] == "RUNNING"):
                    current = i
                    break

            self.session_running = True
            status_text = "Session running... Finished " + str(current) + " of " + str(count)
            self.status_bar.showMessage(status_text)
            self.toggleButton()


    def handleSessionFinished(self, json_result):
        print("Session Finished")
        self.toggleButton()
        task_list = json_result['tasks']

        for i in range(len(task_list)):
            task = task_list[i]
            if(task['kind'] == "CheckRulesTask"):
                path = task['rules'][0]['path'][1:]
                task = {'task_path': path, 'count': task['count'], 'total': task['total']}

                task_report_data = {"session_path": "/QGIS_Session/Auto_Session", "task": task, "task_index": i}
                self.emit( QtCore.SIGNAL('request_task_report(PyQt_PyObject)'), task_report_data)


    def exportLayer(self, layer):
        """
        Convert a QGIS Layer into ESRI Shapefile, save it as a ZIP file, and upload it to 1Integrate
        """
        data_directory = '%s%s%s' % (self.plugin_path, os.sep, 'data')
        layer_name = layer.name().replace(" ","_")
        layer_crs = layer.crs()

        # Save the layer as an ESRI Shapefile
        layer_path = '%s%s%s' % (data_directory, os.sep, layer_name)
        error = QgsVectorFileWriter.writeAsVectorFormat(layer, layer_path, "utf-8", layer_crs, "ESRI Shapefile")
        if error == QgsVectorFileWriter.NoError:
            print("Layer saved on disk")
        else:
            print("Error while saving file")
            return "Error"

        # create a list of all files created while saving the shp
        file_list = []
        for f in os.listdir(data_directory):
            print(f)
            if(f != ".gitignore"):
                file_path = '%s%s%s%s%s' % (self.plugin_path, os.sep, 'data', os.sep, f)
                file_list.append({'path': file_path, 'name': f})

        # save all necessary files in a ZIP
        zip_name = layer_name+".zip"
        zip_path = '%s%s%s%s%s' % (self.plugin_path, os.sep, 'data', os.sep, zip_name)
        zip_file = zipfile.ZipFile(zip_path, mode='w')
        try:
            for i in range(len(file_list)):
                extension = file_list[i]['name'].split(".")[-1]
                if(extension in ['shp','shx','dbf','prj']):
                    zip_file.write(file_list[i]['path'], arcname=file_list[i]['name'])
        finally:
            print("ZIP created")
            zip_file.close()
            for i in range(len(file_list)):
                os.remove(file_list[i]['path'])     # remove the files

        # create the datastore and upload it
        datastore_url = "datastores/QGIS_Datastore/"+layer_name
        datastore = self.getDatastoreTemplate()

        exists = self.checkExists(datastore_url)
        if(exists):
            os.remove(zip_path)
            return datastore_url

        r = self.session.put(self.url % datastore_url, json=datastore)
        r.raise_for_status()
        if r.status_code != requests.codes.ok:
            print("Error while uploading datastore")
            os.remove(zip_path)
            return "Error"

        # The 'requests' library can not upload file properly, so 'urllib2' is necessary
        print("Uploading ZIP")
        try:
            with open(zip_path, 'rb') as payload:
                data = payload.read()
                url = str(self.url % datastore_url)
                cookie = str(self.session.headers['cookie'])

                request = urllib2.Request(url, data, {'Content-Type': 'application/octet-stream', 'Accept': 'application/json', 'Cookie': cookie})
                response = urllib2.urlopen(request)

            r = self.session.put(self.url % datastore_url+"?action=refresh", json={})
            r = self.session.put(self.url % datastore_url+"?action=copyMapping", json={})
        finally:
            os.remove(zip_path)     # delete the ZIP file

        return datastore_url


    def updateAvailableAttributes(self):
        """
        Return a dictionnary of all available attribute in the chosen datastores, with the following format:
        {'class1': ['prop1','prop2'] , 'class2': ['prop1','prop2','prop3']}
        """
        attribute_list = {}
        for datastore in self.session_datastores:
            ds_url = str(datastore['url'])
            r = self.session.get(self.url % ds_url, json={})
            if r.status_code != requests.codes.ok:
                print("Can not get attributes for datastore: "+ds_url)
                continue

            json = r.json()
            if(json.get('importClasses')):
                json = dictToString(json)
                for classes in json['importClasses']:
                    class_name = classes['name']
                    attribute_list[class_name] = []
                    for attrs in classes['attrs']:
                        attribute_list[class_name].append(attrs['name'])

        self.available_attributes = attribute_list


    def findAttributeDifference(self, content):
        """
        Find the difference between the attributes of a specific rule/action content and all available attributes
        Return a dictionnary with missing class, missing property from a missing class, and missing property form an existing class
        """
        queue = []
        difference_list = {'class': [], 'property': { 'missing_class': [], 'existing_class': [] }}
        queue.append(content['rootTerm'])
        while len(queue) > 0:
            item = queue.pop()

            for key in item.keys():
                if key == "classLabel":
                    class_label = item[key]
                    if(not class_label in self.available_attributes.keys()):
                        missing_class = str(class_label)
                        if(not missing_class in difference_list['class']):
                            difference_list['class'].append(missing_class)

                elif key == "propName":
                    prop_name = item[key]
                    class_ref = item['classRef']
                    if(class_ref in self.available_attributes.keys()):
                        if(not prop_name in self.available_attributes[class_ref]):
                            missing_property = str(prop_name+":"+class_ref)
                            if(not missing_property in difference_list['property']['existing_class']):
                                difference_list['property']['existing_class'].append(missing_property)
                    else:
                        missing_property = str(prop_name+":"+class_ref)
                        if(not missing_property in difference_list['property']['missing_class']):
                            difference_list['property']['missing_class'].append(missing_property)

                if type(item[key]) is dict:
                    queue.append(item[key])
                elif type(item[key]) is list:
                    for element in item[key]:
                        queue.append(element)

        return difference_list


    def getAvailableLayers(self):
        available_layers = []
        for layer in self.iface.legendInterface().layers():
            layer_name = str(layer.name())
            if(not layer_name in self.used_layers):
                available_layers.append( str(layer.name()) )

        return available_layers


    def getQgisLayer(self, layer_names):
        result = []
        layer_list = self.iface.legendInterface().layers()
        for layer in layer_list:
            for name in layer_names:
                if(layer.name() == name):
                    result.append(layer)
                    break
        return result


    def getDatastoreTemplate(self):
        datastore = {
          "optLock": 1,
          "importType": "ESRI Shape (GDAL)",
          "importCredentials": {
            "_connection_use": "import",
            "Coordinate Reference System": "",
            "Allow invalid geometries": "true",
            "Source Files (.shp,.shx,.dbf)": "input.zip"
          },
          "exportType": "ESRI Shape (GDAL)",
          "exportCredentials": {
            "_connection_use": "export",
            "Destination Files (.shp,.shx,.dbf)": ""
          }
        }
        return datastore

    def checkExists(self, url):
        """
        Test if an element already exist in 1Integrate (rule, action, session, etc)
        """
        r = self.session.get(self.url % url, json={})
        if(r.status_code == requests.codes.ok):
            print("Element already exist: " + url)
            return True
        else:
            return False


    def findElementByName(self, array, name, parent_name):
        for element in array:
            if(element['name'] == name and element['parent'] == parent_name):
                return element
        return None

    def findElementByUrl(self, array, url):
        for element in array:
            if(element['url'] == url):
                return element
        return None



class InfoThread(QtCore.QThread):
    """
    QThread to get all the available rules. Very similar to the OverviewThread
    """
    def __init__(self,session,url):
        """
        init the object with the session, and the url
        """
        super(InfoThread, self).__init__()

        self.session = session
        self.url = url
        self.arg_list = ["rules","actions"]    # this list contain the arguments added to the request url

    def __del__(self):
        self.wait()


    def run(self):
        while(len(self.arg_list) > 0):
            arg_list = [self.arg_list[i] for i in range(len(self.arg_list))]    # create a shallow copy of the array
            for arg in arg_list:
                self.loadData(arg)


    def loadData(self, arg):
        r = self.session.get(self.url % arg)
        if r.status_code == requests.codes.ok:

            self.arg_list.remove(arg)   # remove the current url from the list

            json = r.json()
            if(json.get('contents')):
                json_content = json['contents']

                for content in json_content:
                    temp_arg = arg + '/' + str(content['name'])

                    if(content['type'] == 'folder' and content['name'] != 'Recycle Bin'):
                        self.arg_list.append(temp_arg)
                        self.emit( QtCore.SIGNAL('new_info(QString, PyQt_PyObject)'), temp_arg, None)

                    else:
                        r = self.session.get(self.url % temp_arg)
                        if r.status_code == requests.codes.ok:
                            json = r.json()
                            self.emit( QtCore.SIGNAL('new_info(QString, PyQt_PyObject)'), temp_arg, json)

class UpdateThread(QtCore.QThread):
    """
    QThread to get updates on a session status and tasks status
    """
    def __init__(self, connection, url, session_path):
        """
        init the object with the connection to the API, the url, and the current session inspected
        """
        super(UpdateThread, self).__init__()

        self.connection = connection    # conection is the connection session to access the API
        self.url = url      # the url of the 1 Integrate API
        self.session_path = session_path        # the path to the the session to inspect
        self.updating = True

    def __del__(self):
        self.wait()


    def run(self):
        """
        Default function containing the main logic of the thread
        """
        while(self.updating):
            self.getUpdate()
            self.sleep(1)

    def stop(self):
        self.updating = False

    def getUpdate(self):
        """
        Execute a request to the API and send the result with a signal
        """
        arg = "results" + self.session_path
        r = self.connection.get(self.url % arg)
        if r.status_code == requests.codes.ok:
            json = r.json()
            if(json.get('tasks')):
                self.emit( QtCore.SIGNAL('new_update(PyQt_PyObject)'), json)
