# -*- coding: utf-8 -*-

import os
import requests

from PyQt4 import QtGui
from PyQt4 import QtCore
from qgis.core import *

from QGIS_Integrate.utility import StaticPath, loadUiClass


FORM_CLASS = loadUiClass('task_report_dialog.ui')

class TaskReportDialog(QtGui.QDialog, FORM_CLASS):

    def __init__(self, iface, session, url):
        """Constructor"""

        super(TaskReportDialog, self).__init__()
        self.setupUi(self)

        self.iface = iface
        self.session = session
        self.url = url

        self.task_report_list = []

        self.button_remove.clicked.connect(self.removeReport)
        self.button_zoom.clicked.connect(self.zoomOnFeature)
        self.task_report_tree.itemSelectionChanged.connect(self.handleSelectedItem)


    def addReport(self, json_data):
        """
        Add a new report to the list of task report, create a layer with the reports points, and create a new element in the tree widget
        The boolean return value indicate success or failure during the whole process
        """
        session_path = json_data['session_path']
        task_detail = json_data['task']
        task_index = json_data['task_index']

        arg = "results" + session_path + "?detail=report&taskIndex=" + str(task_index)
        r = self.session.get(self.url % arg)

        if(r.status_code != requests.codes.ok):
            print("Request error on: "+arg)
            return False

        json_report = r.json()

        if(len(json_report) < 1):
            print("Task report is empty:")
            print(json_report)

            text = "Error: The plugin is unable to find a report for the chosen task \n \
            Cause: The task order inside the session and the label of each task does not match \n\n \
            Do you want to attempt to fix this error ? You will loose all session progress"
            result = QtGui.QMessageBox.warning(self, "Empty Task Report", text, QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)

            if(result == QtGui.QMessageBox.Yes):
                success = self.recreateSession(session_path)
                if(success == False):
                    msg = QtGui.QMessageBox.critical(self, "Error", "Error fix failed")
                    return False
            else:
                return False

        layer_name = session_path[1:] + " Task_" + str(task_index)
        text, success = QtGui.QInputDialog.getText(self, 'Task Report', 'Enter a name for the report layer:', text=layer_name)
        if(success and not text == ""):
            layer_name = text

        # create a new layer, define the layer style, and get the dataProvider to add features to the layer
        uri = "Point?crs=epsg:4326&field=ID:int&field=name:string&field=description:string"
        report_layer = QgsVectorLayer(uri, layer_name, 'memory')
        provider = report_layer.dataProvider()

        symbol_layer = QgsSvgMarkerSymbolLayerV2('symbol/red-marker.svg', size=10)
        symbol_layer.setVerticalAnchorPoint(2)
        report_layer.rendererV2().symbols()[0].changeSymbolLayer(0, symbol_layer)

        # create a new top level element in the QTreeWidget, define the name and font (bold)
        parent_item = QtGui.QTreeWidgetItem()
        parent_item.setText(0, layer_name)
        font = QtGui.QFont()
        default_family = font.defaultFamily()
        parent_item.setFont(0, QtGui.QFont(default_family, -1, QtGui.QFont.Bold, False))

        # create a dict that will contains all the informations relative to the task report
        task_report_data = {'session_path': session_path[1:], 'task_detail': task_detail, 'layer_name': layer_name}

        feature_list = []
        feature_count = 0
        for i in range(len(json_report)):       # add all features to the layer
            report_item = json_report[i]

            # get all necessary properties from the json report
            gothic_id = int(report_item['gothicId'].encode('ascii', 'ignore'))
            class_name = report_item['className'].encode('ascii', 'ignore')
            feature_id = ""
            if(report_item['attributes'].get('ID')):
                feature_id = report_item['attributes']['ID']['value'].encode('ascii', 'ignore')
            description = report_item['nonconformances'][0]['description'].encode('ascii', 'ignore')

            # define the position of the report feature
            coord_list = []

            if(report_item['attributes'].get('geometry')):      # the coordinates can be in the attribute part
                if(report_item['attributes']['geometry'].get('x1')):
                    x = ( float(report_item['attributes']['geometry']['x1']) + float(report_item['attributes']['geometry']['x0']) ) * 0.5
                    y = ( float(report_item['attributes']['geometry']['y1']) + float(report_item['attributes']['geometry']['y0']) ) * 0.5
                    coord_list.append({'x': x, 'y': y})

                elif(report_item['attributes']['geometry'].get('x0')):
                    x = report_item['attributes']['geometry']['x0']
                    y = report_item['attributes']['geometry']['y0']
                    coord_list.append({'x': x, 'y': y})

            elif("Hotspot" in description):     # or they can be a hotspot in the description...
                hotspot_coord = description.split("\n")[-1].split(")")
                for j in range(len(hotspot_coord)):

                    xy = hotspot_coord[j][1:].split(",")
                    if(xy[0] != ''):
                        x = float(xy[0])
                        y = float(xy[1])
                        coord_list.append({'x': x, 'y': y})

            for j in range(len(coord_list)):
                error_id = gothic_id + j
                # create the feature
                feature = QgsFeature()
                feature.setGeometry(QgsGeometry.fromPoint(QgsPoint(x,y)))
                feature.setAttributes([error_id, class_name, description])
                provider.addFeatures([feature])
                feature_count += 1

                # create the QTreeWidget item
                tree_item_name = "Error: " + str(error_id)
                tree_item = QtGui.QTreeWidgetItem()
                tree_item.setText(0, tree_item_name)
                parent_item.addChild(tree_item)

                descript = description.split("\n")[0].replace("_","")
                feature_list.append({'list_id': feature_count, 'error_id': error_id, 'feature_id': feature_id, 'class_name': class_name, 'x': x, 'y': y, 'description': descript})


        if(len(feature_list) == 0):     # if there is no hotspot, do not create a new report but show an information message
            msg = QtGui.QMessageBox.critical(self, "Layer Creation Error", "Error: No hotspot found to create a layer\nCaused by: The task report does not have a hotspot geometry attribute")
            print(description)
            return False

        # add the new layer to the LayerRegistry
        report_layer.updateExtents()
        QgsMapLayerRegistry.instance().addMapLayer(report_layer)

        # add the dict with all data in the list
        task_report_data['feature_list'] = feature_list
        self.task_report_list.append(task_report_data)

        # add the TreeWidget item to the widget
        self.task_report_tree.addTopLevelItem(parent_item)
        parent_item.setExpanded(True)

        return True


    def removeReport(self):
        """
        Remove the selected report from the tree widget
        """
        current_item = self.task_report_tree.currentItem()
        if(current_item.parent() != None):
            return

        layer_name = current_item.text(0)

        # remove the report from the list of objects
        idx = -1
        for i in range(len(self.task_report_list)):
            if(self.task_report_list[i]['layer_name'] == layer_name):
                idx = i
                break
        if(idx != -1):
            del self.task_report_list[idx]

        # remove the report from the QTreeWidget
        idx = self.task_report_tree.indexOfTopLevelItem(current_item)
        if(idx != -1):
            self.task_report_tree.takeTopLevelItem(idx)

        # remove the report layer from the layer registry
        layer_registry = QgsMapLayerRegistry.instance().mapLayers()
        for key in layer_registry:
            if(layer_registry[key].name() == layer_name):
                QgsMapLayerRegistry.instance().removeMapLayer(key)
                break

        self.updateUI(-1,-1)
        self.button_remove.setEnabled(False)


    def handleSelectedItem(self):
        """
        Event listener when a tree widget element is clicked
        """
        tree_widget_item = self.task_report_tree.currentItem()
        if(tree_widget_item == None):
            return

        parent = tree_widget_item.parent()
        if(parent == None):
            self.button_remove.setEnabled(True)
            self.button_zoom.setEnabled(False)
            task_idx = self.task_report_tree.indexOfTopLevelItem(tree_widget_item)
            feature_idx = -1
        else:
            self.button_remove.setEnabled(False)
            self.button_zoom.setEnabled(True)
            task_idx = self.task_report_tree.indexOfTopLevelItem(parent)
            feature_idx = parent.indexOfChild(tree_widget_item)

        self.updateUI(task_idx, feature_idx)


    def updateUI(self, task_idx, feature_idx):
        """
        Update the text of the UI depending on the clicked element
        """
        if(task_idx != -1):
            task_report = self.task_report_list[task_idx]

            string = task_report['session_path']
            self.label_session_name.setText("<b>Session: </b>" + string)

            string = "/".join(task_report['task_detail']['task_path'].split("/")[1:])
            self.label_task_name.setText("<b>Rule: </b>" + string)

            total = task_report['task_detail']['total']
            self.label_object_count.setText("<b>Objects: </b>" + str(total))

            count = task_report['task_detail']['count']
            percent = 100 * (total-count) / total
            string = "%0.2f%%" % (percent)
            self.label_conformance.setText("<b>Conformance: </b>" + string)
        else:
            self.label_session_name.setText("<b>Session: </b>")
            self.label_task_name.setText("<b>Rule: </b>")
            self.label_object_count.setText("<b>Objects: </b>")
            self.label_conformance.setText("<b>Conformance: </b>")

        if(feature_idx != -1):
            feature = task_report['feature_list'][feature_idx]

            string = str(feature['list_id']) + " of " + str(len(task_report['feature_list']))
            self.label_list_id.setText("<b>Feature:</b> " + string)

            string = feature['class_name']
            self.label_feature_class.setText("<b>Class:</b> " + string)

            string = feature['feature_id']
            self.label_feature_id.setText("<b>ID:</b> " + string)

            string = feature['description']
            self.label_feature_description.setText("<b>Decription:</b> " + string)

            string = "x: " + str(feature['x']) + ", y: " + str(feature['y'])
            self.label_feature_hotspot.setText("<b>Hotspot:</b> " + string)
        else:
            self.label_list_id.setText("<b>Feature: </b> ")
            self.label_feature_class.setText("<b>Class: </b> ")
            self.label_feature_id.setText("<b>ID: </b> ")
            self.label_feature_description.setText("<b>Decription: </b> ")
            self.label_feature_hotspot.setText("<b>Hotspot: </b> ")


    def zoomOnFeature(self):
        """
        Get the item selected from the tree widget, get the corresponding layer and layer feature, and zoom on it
        """
        current_item = self.task_report_tree.currentItem()
        parent_item = current_item.parent()
        layer_name = parent_item.text(0)

        feature_id = float(current_item.text(0).split(":")[-1])

        current_layer = -1
        layer_registry = QgsMapLayerRegistry.instance().mapLayers()
        for key in layer_registry:
            if(layer_registry[key].name() == layer_name):
                current_layer = layer_registry[key]
                break

        if(current_layer != -1):
            legend = self.iface.legendInterface()
            if(not legend.isLayerVisible(current_layer)):
                legend.setLayerVisible(current_layer, True)

            iterable = current_layer.getFeatures()
            for feature in iterable:
                attr = feature.attributes()
                idx = attr[0]
                if(idx == feature_id):
                    current_layer.setSelectedFeatures([feature.id()])
                    break

            self.iface.mapCanvas().zoomToSelected(current_layer)
            # current_layer.setSelectedFeatures([])

    def recreateSession(self, session_path):
        """
        Try to delete a session and recreate it, to fix the un-ordered task problem (label does not match task order)
        """
        arg = "sessions" + session_path
        r = self.session.get(self.url % arg)
        if(r.status_code != requests.codes.ok):
            return False

        json  = r.json()
        r = self.session.delete(self.url % arg)
        if(r.status_code != requests.codes.ok):
            return False

        r = self.session.put(self.url % arg, json=json)
        if(r.status_code != requests.codes.ok):
            return False

        return True
