# -*- coding: utf-8 -*-

import os
import requests

from PyQt4 import QtGui
from PyQt4 import QtCore
from qgis.core import *

from QGIS_Integrate.utility import StaticPath, loadUiClass, QTaskWidget


FORM_CLASS = loadUiClass('session_dialog.ui')

class SessionDialog(QtGui.QDialog, FORM_CLASS):

    def __init__(self, connection, url):
        """
        Constructor
        The request object 'session' has been renamed 'connection' to avoid confusion with the 1Integrate Session and all associated variables
        """

        super(SessionDialog, self).__init__()
        self.setupUi(self)
        self.plugin_path = StaticPath().path
        self.setWindowIcon( QtGui.QIcon('%s%simg%s%s' % (self.plugin_path, os.sep, os.sep, 'favicon.png')) )
        self.setWindowTitle("Session")

        self.connection = connection
        self.url = url
        self.session_path = None

        self.session_thread = None
        self.update_thread = None
        self.session_status = None

        # create icons
        self.button_start.setIcon( QtGui.QIcon('%s%simg%s%s' % (self.plugin_path, os.sep, os.sep, 'icon_start.png')) )
        self.button_pause.setIcon( QtGui.QIcon('%s%simg%s%s' % (self.plugin_path, os.sep, os.sep, 'icon_pause.png')) )
        self.button_stop.setIcon( QtGui.QIcon('%s%simg%s%s' % (self.plugin_path, os.sep, os.sep, 'icon_stop.png')) )

        # initialize all event listener
        self.button_start.clicked.connect(self.startSession)
        self.button_pause.clicked.connect(self.pauseSession)
        self.button_stop.clicked.connect(self.stopSession)

        # self.connect(self, QtCore.SIGNAL("showFinished()"), self.startThread)
        self.task_list_widget.itemClicked.connect(self.handleItemSelection)
        self.button_view_result.clicked.connect(self.requestTaskReport)

        self.task_list = []
        self.show_error = True


    def show(self):
        """
        Default function, called when the widget is drawn on the screen
        """
        super(SessionDialog, self).show()


    def closeEvent(self, event):
        if(self.session_thread != None):
            self.session_thread.quit()
        if(self.update_thread != None):
            self.update_thread.thread_running = False
            self.update_thread.quit()


    def initWidget(self, session_path):
        """
        Create a new thread to execute request on the 1Integrate API, and prevent blocking the UI
        """
        self.session_path = session_path
        string = self.session_path.split("/")[-1]
        self.label_name.setText("<b>Session: </b>" + string)

        self.task_list = []
        self.task_list_widget.clear()
        self.show_error = True

        self.session_thread = SessionThread(self.connection, self.url, self.session_path)
        self.connect(self.session_thread, QtCore.SIGNAL("session_thread_finished(PyQt_PyObject)"), self.handleSessionInfo)
        self.session_thread.start()

        self.update_thread = UpdateThread(self.connection, self.url, self.session_path)
        self.connect(self.update_thread, QtCore.SIGNAL("update_thread_new_update(PyQt_PyObject)"), self.handleUpdateInfo)
        self.update_thread.start()


    def resetWidget(self):
        if(self.session_thread != None):
            self.session_thread.quit()
        if(self.update_thread != None):
            self.update_thread.thread_running = False
            self.update_thread.quit()

        self.session_path = None
        self.label_name.setText("<b>Session: </b>")

        self.task_list = []
        self.task_list_widget.clear()


    def handleSessionInfo(self, json):
        """
        Event listener triggered when the API request to the session finished
        Receive the list of tasks contained in the session and put it in the QListWidget
        """
        arg = "sessions" + self.session_path
        r = self.connection.put(self.url % arg, json=json)
        if(r.status_code != requests.codes.ok):      # update the session with itself, this way the taskIndex are sorted in order
            print("Session update failed")          # if the update fail, the widget will still show the list of task

        task_list = json['tasks']
        for task in task_list:
            kind = task['kind'].encode('ascii', 'replace')      # find the kind of task

            detail = " "
            if(kind == 'OpenData' or kind == 'Commit' or kind == 'CopyTo'):     # set details depending on task kind
                detail = task['datastore'].encode('ascii', 'replace')
            elif(kind == 'CheckRules'):
                detail = task['rules'][0].encode('ascii', 'replace')
            elif(kind == 'ApplyActions'):
                detail = task['actions'][0].encode('ascii', 'replace')
            elif(kind == 'ApplyActionMap"'):
                detail = task['actionmap'].encode('ascii', 'replace')
            detail = detail[1:]

            self.task_list.append({'kind': kind, 'task_path': detail, 'status': None, 'total': None, 'count': None})      # add the task to the list of tasks

            task_widget = QTaskWidget()     # create a new custom widget
            task_widget.setKindText(kind)
            task_widget.setDetailText(detail)

            task_list_item = QtGui.QListWidgetItem(self.task_list_widget)     # create a QListWidgetItem to contain the custom widget
            task_list_item.setSizeHint(task_widget.sizeHint())

            self.task_list_widget.addItem(task_list_item)     # add the list item to the list
            self.task_list_widget.setItemWidget(task_list_item, task_widget)      # link the list item with the custom item



    def handleUpdateInfo(self, json):
        """
        Update the UI everytime a request is made by the SessionThread on the current session status

        Because the 'Pause' task does not exist in the variable 'json_task_list' received on every update,
        we use a variable 'json_idx' to match task from the 'json_task_list' with task from the 'self.task_list'
        """
        json_task_list = json['tasks']
        json_idx = -1
        for i in range(len(self.task_list)):
            task_list_item = self.task_list_widget.item(i)        # get the custom widget
            task_widget = self.task_list_widget.itemWidget(task_list_item)

            if(self.task_list[i]['kind'] == "Pause"):      # test for pause task status
                if(i < len(self.task_list)-1):
                     if(self.task_list[i+1]['status'] == "NOT_STARTED" or self.task_list[i+1]['status'] == None):       # if the next task has not started yet
                         current_status = "NOT_STARTED"
                     else:
                         current_status = "FINISHED"
                elif(json['status'] == "FINISHED"):     # test for pause task if it is the last task of the session
                    current_status = "FINISHED"
                else:
                    current_status = "NOT_STARTED"

                self.task_list[i]['status'] = current_status
                task_widget.setTaskStatus(current_status)

            else:
                json_idx += 1
                current_status = json_task_list[json_idx]['status']
                self.task_list[i]['status'] = current_status
                self.task_list[i]['total'] = json_task_list[json_idx]['total']
                self.task_list[i]['count'] = json_task_list[json_idx]['count']

                task_widget.setTaskStatus(current_status)        # update every item inside the QTreeWidget
                task_widget.updateValues(json_task_list[json_idx]['total'], json_task_list[json_idx]['processed'], json_task_list[i]['count'])


        if(json.get('errors')):     # if the session has an error, show it in a message box
            self.label_status.setText("<b>Status:</b> ERROR")
            self.session_status = "PAUSED"
            self.toggleButton()

            if(self.show_error):    # this condition avoid showing the message on each update (every second)
                self.show_error = False     # change the state so the message is shown only once
                message = json['errors'][0]['message']
                error_reply = QtGui.QMessageBox.critical(self, 'Session Error', message)

        else:       # if everything is fine, update the bottom text and the buttons states
            status = str(json['status'])
            self.session_status = status
            self.toggleButton()

            text = "<b>Status: </b>" + status.replace("_", " ")
            self.label_status.setText(text)


    def startSession(self):
        arg = "sessions" + self.session_path + "?action=play"
        r = self.connection.put(self.url % arg, json={})

        if r.status_code == requests.codes.ok:
            self.session_status = None
            self.show_error = True
            self.toggleButton()

    def pauseSession(self):
        arg = "sessions" + self.session_path + "?action=pause"
        r = self.connection.put(self.url % arg, json={})

        if r.status_code == requests.codes.ok:
            self.session_status = None
            self.toggleButton()

    def stopSession(self):
        arg = "sessions" + self.session_path + "?action=stop"
        r = self.connection.put(self.url % arg, json={})

        if r.status_code == requests.codes.ok:
            self.session_status = None
            self.toggleButton()


    def toggleButton(self):
        """
        Enable and disable UI buttons depending on the current session status
        """
        if(self.session_status == "NOT_STARTED"):
            self.button_start.setEnabled(True)
            self.button_pause.setEnabled(False)
            self.button_stop.setEnabled(False)
        elif(self.session_status == "RUNNING"):
            self.button_start.setEnabled(False)
            self.button_pause.setEnabled(True)
            self.button_stop.setEnabled(True)
        elif(self.session_status == "PAUSED"):
            self.button_start.setEnabled(True)
            self.button_pause.setEnabled(False)
            self.button_stop.setEnabled(True)
        elif(self.session_status == "FINISHED"):
            self.button_start.setEnabled(False)
            self.button_pause.setEnabled(False)
            self.button_stop.setEnabled(True)
        else:
            self.button_start.setEnabled(False)
            self.button_pause.setEnabled(False)
            self.button_stop.setEnabled(False)


    def handleItemSelection(self, task_list_item):
        """
        Change the state of the 'View Result' button when a item of the list is clicked
        """
        idx = self.task_list_widget.row(task_list_item)
        task = self.task_list[idx]
        if(task['kind'] == "CheckRules" and task['status'] == "FINISHED"):
            self.button_view_result.setEnabled(True)
        else:
            self.button_view_result.setEnabled(False)


    def requestTaskReport(self):
        task_list_item = self.task_list_widget.selectedItems()[0]     # get the clicked list element
        idx = self.task_list_widget.row(task_list_item)
        task = self.task_list[idx]

        task_report_data = {"session_path": self.session_path, "task": task, "task_index": idx}
        self.emit( QtCore.SIGNAL('request_task_report(PyQt_PyObject)'), task_report_data)



class SessionThread(QtCore.QThread):
    """
    QThread to request the informations of a session
    """
    def __init__(self, connection, url, session_path):
        """
        init the object with the connection to the API, the url, and the current session inspected
        """
        super(SessionThread, self).__init__()

        self.connection = connection    # conection is the connection session to access the API
        self.url = url      # the url of the  API
        self.session_path = session_path        # the path to the the session to inspect

    def __del__(self):
        self.wait()

    def run(self):
        """
        Default function containing the main logic of the thread, called by SessionThread.start()
        """
        arg = "sessions" + self.session_path
        r = self.connection.get(self.url % arg)
        if r.status_code == requests.codes.ok:
            json = r.json()
            self.emit( QtCore.SIGNAL('session_thread_finished(PyQt_PyObject)'), json)


class UpdateThread(QtCore.QThread):
    """
    QThread to get updates on a session status and tasks status
    """
    def __init__(self, connection, url, session_path):
        """
        init the object with the connection to the API, the url, and the current session inspected
        """
        super(UpdateThread, self).__init__()

        self.connection = connection    # conection is the connection session to access the API
        self.url = url      # the url of the 1 Integrate API
        self.session_path = session_path        # the path to the the session to inspect
        self.thread_running = True

    def __del__(self):
        self.wait()

    def run(self):
        """
        Default function containing the main logic of the thread
        """
        while(self.thread_running):
            self.getUpdate()
            self.sleep(1)

    def getUpdate(self):
        """
        Execute a request to the API and send the result with a signal
        """
        arg = "results" + self.session_path
        r = self.connection.get(self.url % arg)
        if r.status_code == requests.codes.ok:
            json = r.json()
            if(json.get('tasks')):
                self.emit( QtCore.SIGNAL('update_thread_new_update(PyQt_PyObject)'), json)
