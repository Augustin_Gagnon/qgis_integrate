# -*- coding: utf-8 -*-

import os
import math
import base64

from PyQt4 import QtGui
from PyQt4 import QtCore
from qgis.gui import QgsMessageBar

from QGIS_Integrate.utility import StaticPath, loadPackage, loadUiClass

loadPackage("requests")
import requests
# try:
#     import requests
# except ImportError:
#     loadPackage("requests")
#     import requests

FORM_CLASS = loadUiClass('connection_dialog.ui')

class ConnectionDialog(QtGui.QDialog, FORM_CLASS):

    def __init__(self, iface):
        """Constructor"""
        super(ConnectionDialog, self).__init__()
        self.setupUi(self)

        self.iface = iface
        self.plugin_path = StaticPath().path
        self.setWindowIcon( QtGui.QIcon('%s%simg%s%s' % (self.plugin_path, os.sep, os.sep, 'favicon.png')) )

        self.url = None
        self.session = None
        self.button_connect.clicked.connect(self.startConnection)

        self.connect(self, QtCore.SIGNAL("showFinished()"), self.fillPlaceholder)


    def show(self):
        super(ConnectionDialog, self).show()
        self.emit( QtCore.SIGNAL('showFinished()') )


    def fillPlaceholder(self):
        """
        Fill the placeholder of each QLineEdit
        """
        self.getLogin()
        self.input_url.setPlaceholderText("http://localhost:8080/1Integrate/")
        self.input_username.setPlaceholderText("")
        self.input_password.setPlaceholderText("")


    def startConnection(self):
        # get the user input
        username = self.input_username.text()
        password = self.input_password.text()
        url = self.input_url.text()

        if(username != "" and password != "" and url != ""):
            self.saveLogin(username,password,url)

            if(url[-1] != "/"):
                url += "/"
            self.url = url + "rest/%s"

            try:
                self.session = requests.Session()
                r = self.session.post(self.url % 'token', json={'username': username, 'password': password})

                if(r.status_code == requests.codes.ok):
                    self.session.headers.update({'Accept': 'Application/JSON', 'Content-Type': 'Application/JSON'})
                    self.accept()       # go back to the main plugin

                else:
                   string = 'Unable to connect to the URL - status: ' + str(r.status_code)
                   msg = QtGui.QMessageBox.critical(self, "Connection Error", string)

            except IOError as e:
                # msg = QtGui.QMessageBox.critical(self, "IO Error", str(e))
                print(e)

        else:
            if(username == ""):
               self.input_username.setPlaceholderText("No Empty value !")
            if(password == ""):
               self.input_password.setPlaceholderText("No Empty value !")


    def saveLogin(self,username,password,url):
        if(username != "" or password != "" or address != ""):      #don't try to read file for nothing

            path = '%s%s%s' % (self.plugin_path, os.sep, 'save.txt')
            try:
                with open(path, "r+") as f:
                    saved_info = f.readline().split("_;_")

                    if(self.checkbox_save_login.isChecked()):
                        saving_name = username if(username != "") else("%&name%")
                        saving_pasw = base64.b64encode(password) if(password != "") else("%&pasw%")
                    else:
                        saving_name = saved_info[0]
                        saving_pasw = saved_info[1]

                    if(self.checkbox_save_url.isChecked()):
                        saving_url = url if(url != "") else("%&url%")
                    else:
                        saving_url = saved_info[2]

                    f.seek(0)       # place the cursor at the first character in the text file
                    f.truncate()    # and erase everything

                    f.write(saving_name)
                    f.write('_;_')
                    f.write(saving_pasw)
                    f.write('_;_')
                    f.write(saving_url)

                    f.close()

            except IOError as e:
                message = "Could not save login information (IOError %s): %s \n Please make sure the following folder have read/write permission: %s" % (e.errno, e.strerror, self.plugin_path)
                msg = QtGui.QMessageBox.warning(self, "IO Error", message)


    def getLogin(self):
        path = '%s%s%s' % (self.plugin_path, os.sep, 'save.txt')
        username = ""
        password = ""
        url = "";
        with open(path, "r") as f:
            try:
                info = f.readline().split('_;_')
                saved_name = info[0]
                saved_pasw = info[1]
                saved_url = info[2]

                username = saved_name if(saved_name != "%&name%") else("")
                password = base64.b64decode(saved_pasw) if(saved_pasw != "%&pasw%") else("")
                url = saved_url if(saved_url != "%&url%") else("")

                f.close()

            except IOError as e:
                message = "Could not get saved login information (IOError %s): %s" % (e.errno, e.strerror)
                self.iface.messageBar().pushMessage("ERROR", message, level=QgsMessageBar.INFO)

        self.input_username.setText(username)
        self.input_password.setText(password)
        self.input_url.setText(url)
