# -*- coding: utf-8 -*-

import os
from datetime import datetime
import requests
# from functools import partial

from PyQt4 import QtGui
from PyQt4 import QtCore
# from qgis.core import *

from QGIS_Integrate.utility import StaticPath, loadPackage, loadUiClass

from QGIS_Integrate.dialogs.overview_dialog import OverviewDialog
from QGIS_Integrate.dialogs.creation_dialog import CreationDialog
from QGIS_Integrate.dialogs.task_report_dialog import TaskReportDialog




FORM_CLASS = loadUiClass('main_dialog.ui')

class MainDialog(QtGui.QMainWindow, FORM_CLASS):

    def __init__(self, iface, session, url):
        """Constructor"""
        super(MainDialog, self).__init__()
        self.setupUi(self)
        self.iface = iface
        self.plugin_path = StaticPath().path
        self.setWindowIcon( QtGui.QIcon('%s%simg%s%s' % (self.plugin_path, os.sep, os.sep, 'favicon.png')) )

        # variable for API requests
        self.session = session
        self.url = url

        # define the menu icons and actions
        self.action_reconnect.triggered.connect(self.requestReconnectUser)
        self.action_reconnect.setIcon( QtGui.QIcon('%s%simg%s%s' % (self.plugin_path, os.sep, os.sep, 'icon_reconnect.png')) )
        self.action_disconnect.triggered.connect(self.requestDisconnectUser)
        self.action_disconnect.setIcon( QtGui.QIcon('%s%simg%s%s' % (self.plugin_path, os.sep, os.sep, 'icon_disconnect.png')) )

        # define the children widgets
        self.creation_dialog = CreationDialog(self.iface, self.status_bar, self.session, self.url)
        self.overview_dialog = OverviewDialog(self.iface, self.status_bar, self.session, self.url)
        self.task_report_dialog = TaskReportDialog(self.iface, self.session, self.url)

        # set the QStackedWidget
        self.stacked_widget.insertWidget(0, self.creation_dialog)
        self.stacked_widget.insertWidget(1, self.overview_dialog)

        # set the dockable widget
        self.dock_widget = QtGui.QDockWidget("Task Report", self.iface.mainWindow())
        self.dock_widget.setAllowedAreas(QtCore.Qt.LeftDockWidgetArea | QtCore.Qt.RightDockWidgetArea)
        self.dock_widget.setWidget(self.task_report_dialog)

        self.connect(self.creation_dialog, QtCore.SIGNAL("request_task_report(PyQt_PyObject)"), self.handleNewReport)
        self.connect(self.overview_dialog, QtCore.SIGNAL("request_task_report(PyQt_PyObject)"), self.handleNewReport)

        self.button_switch.clicked.connect(self.handleSwitch)


    def show(self):
        super(MainDialog, self).show()
        self.toggleWidget()


    def closeEvent(self, event):
        """
        Function called when the widget is closed
        """
        self.overview_dialog.close()
        self.creation_dialog.close()
        self.task_report_dialog.close()
        self.dock_widget.close()


    def handleSwitch(self):
        """
        Event listener, triggered by the "Basic/Advanced" button
        """
        idx = self.stacked_widget.currentIndex()
        if(idx == 0):
            self.stacked_widget.setCurrentIndex(1)
        elif(idx == 1):
            self.stacked_widget.setCurrentIndex(0)
        self.toggleWidget()


    def toggleWidget(self):
        idx = self.stacked_widget.currentIndex()
        if(idx == 0):
            self.creation_dialog.initWidget()
            if(not self.creation_dialog.isVisible()):
                self.creation_dialog.show()
            self.button_switch.setText("Advanced")

        elif(idx == 1):
            self.overview_dialog.initWidget()
            if(not self.overview_dialog.isVisible()):
                self.overview_dialog.show()
            self.button_switch.setText("Basic")


    def handleNewReport(self, task_report_data):
        success = self.task_report_dialog.addReport(task_report_data)
        if(success and not self.task_report_dialog.isVisible()):
            self.iface.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.dock_widget)


    def requestReconnectUser(self):
        cookie = self.session.headers['cookie']
        self.session = requests.Session()
        self.session.headers.update({'Accept': 'Application/JSON', 'Content-Type': 'Application/JSON', 'Cookie': cookie})


    def requestDisconnectUser(self):
        self.emit( QtCore.SIGNAL('disconnect_user'))
        self.close()

    def center(self):
        """
        This function center the window on the screen
        """
        rect = self.frameGeometry()         #size of the widget
        center = QtGui.QDesktopWidget().availableGeometry().center()      #center of screen resolution
        rect.moveCenter(center)
        self.move(rect.topLeft())
