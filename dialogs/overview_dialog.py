# -*- coding: utf-8 -*-

import os
from datetime import datetime
import requests
# from functools import partial

from PyQt4 import QtGui
from PyQt4 import QtCore
# from qgis.core import *

from QGIS_Integrate.utility import StaticPath, loadPackage, loadUiClass

from QGIS_Integrate.dialogs.session_dialog import SessionDialog


FORM_CLASS = loadUiClass('overview_dialog.ui')

class OverviewDialog(QtGui.QDialog, FORM_CLASS):

    def __init__(self, iface, status_bar, session, url):
        """Constructor"""
        super(OverviewDialog, self).__init__()
        self.setupUi(self)
        self.iface = iface
        self.status_bar = status_bar
        self.plugin_path = StaticPath().path
        self.setWindowIcon( QtGui.QIcon('%s%simg%s%s' % (self.plugin_path, os.sep, os.sep, 'favicon.png')) )

        self.session = session
        self.url = url
        self.overview_thread = None
        self.session_dialog = None

        self.folder_icon = QtGui.QIcon('%s%simg%s%s' % (self.plugin_path, os.sep, os.sep, 'icon_folder.ico'))

        # create arrays to store the data after the request
        self.datastores_list = []
        self.rules_list = []
        self.actions_list = []
        self.actionmaps_list = []
        self.sessions_list = []

        # event listener to display informations when a tree element is clicked
        self.datastores_tree.itemClicked.connect(self.onClickDatastoreItem)
        self.rules_tree.itemClicked.connect(self.onClickRulesItem)
        self.actions_tree.itemClicked.connect(self.onClickActionsItem)
        self.actionmaps_tree.itemClicked.connect(self.onClickActionmapsItem)
        self.sessions_tree.itemClicked.connect(self.onClickSessionsItem)

        # more event listener
        self.button_refresh.clicked.connect(self.handleRefreshButton)
        self.sessions_button_open.clicked.connect(self.openSessionDialog)

        self.month_list = ['Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'June', 'July', 'Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.']


    def show(self):
        """
        Default function, called when the widget is drawn on the screen
        """
        super(OverviewDialog, self).show()


    def closeEvent(self, event):
        """
        Function called when the widget is closed
        """
        if(self.overview_thread != None):
            self.overview_thread.quit()

        if(self.session_dialog != None):
            self.session_dialog.close()


    def initWidget(self):
        self.refreshData(['datastores','rules','actions','actionmaps','sessions'])


    def refreshData(self, menu_list):
        """
        Refresh a tab by starting a request thread
        """
        if("datastores" in menu_list):
            self.datastores_tree.clear()
            self.datastores_list = []
        if("rules" in menu_list):
            self.rules_tree.clear()
            self.rules_list = []
        if("actions" in menu_list):
            self.actions_tree.clear()
            self.actions_list = []
        if("actionmaps" in menu_list):
            self.actionmaps_tree.clear()
            self.actionmaps_list = []
        if("sessions" in menu_list):
            self.sessions_tree.clear()
            self.sessions_list = []

        self.overview_thread = OverviewThread(self.session, self.url, menu_list)
        self.connect(self.overview_thread, QtCore.SIGNAL("new_node(QString, PyQt_PyObject, PyQt_PyObject)"), self.handleNewNode)
        self.connect(self.overview_thread, QtCore.SIGNAL("finished()"), lambda: self.status_bar.clearMessage())
        self.status_bar.showMessage("Loading ...")
        self.overview_thread.start()


    def handleNewNode(self, arg, top_level, content):
        """
        Event listener called when a result from a request to the API is received
        Add the data as a node in one of the QTreeWidget, and add it to one of the data list
        """
        node_menu = arg.split("/")[0]
        node_name = arg.split("/")[-1]
        node_parent = arg.split("/")[-2]

        if(node_name == "Recycle Bin"):      # avoid adding the Recycle Bin to the widget
            return

        if(content == None):
            node_type = "folder"
        else:
            node_type = "leaf"

        if(node_menu == "datastores"):
            tree_widget = self.datastores_tree
            element_list = self.datastores_list
            properties = ['description','created','updated','createdBy','updatedBy','importType','exportType','comments']
        elif(node_menu == "rules"):
            tree_widget = self.rules_tree
            element_list = self.rules_list
            properties = ['description','created','updated','createdBy','updatedBy','comments']
        elif(node_menu == "actions"):
            tree_widget = self.actions_tree
            element_list = self.actions_list
            properties = ['description','created','updated','createdBy','updatedBy','comments']
        elif(node_menu == "actionmaps"):
            tree_widget = self.actionmaps_tree
            element_list = self.actionmaps_list
            properties = ['description','created','updated','createdBy','updatedBy','comments']
        elif(node_menu == "sessions"):
            tree_widget = self.sessions_tree
            element_list = self.sessions_list
            properties = ['created','updated','createdBy','updatedBy','status']


        if(node_type == "leaf"):
            element = {}        # init the new element
            element['name'] = node_name
            for prop in properties:     # add all the required properties, if they exist
                if(content.get(prop)):
                    element[prop] = content[prop]
                else:
                    element[prop] = ""
            element_list.append(element)


        tree_item = QtGui.QTreeWidgetItem()     #create a new QTreeItem to add to the widget
        tree_item.setText(0, node_name)

        if(node_type == 'folder'):       # set the folder icon
            tree_item.setIcon(0, self.folder_icon)

        if(top_level == True):
            tree_widget.addTopLevelItem(tree_item)
        else:
            parent_item = self.findTreeElementByName(tree_widget, node_parent)      # find the the parent node of the current item
            if(parent_item):
                parent_item.addChild(tree_item)



    def onClickDatastoreItem(self, item, column):
        """
        Evenet listener, called when a QTreeWidgetItem of the Datastores QTreeWidget is clicked
        """
        name = item.text(column)        # get the name and number of children, to check if it's a folder
        children = item.childCount()
        if(children == 0 and name != "Recycle Bin"):
            element = self.findListElementByName(self.datastores_list, name)        # get the matching element in the list of element

            if(element):     # update the UI
                string = element['name'].encode('ascii', 'replace')
                self.datastores_label_name.setText("<b>Name: </b>" + string)

                string = element['description'].encode('ascii', 'replace')
                self.datastores_label_description.setText("<b>Description: </b>" + string)

                name = element['createdBy'].encode('ascii', 'replace')
                unix_time = element['created'] * 0.001
                time = datetime.utcfromtimestamp(unix_time)
                string = "%s, %d %s %d" % (name, time.day, self.month_list[time.month], time.year)
                self.datastores_label_created.setText("<b>Created: </b>" + string)

                name = element['updatedBy'].encode('ascii', 'replace')
                unix_time = element['updated'] * 0.001
                time = datetime.utcfromtimestamp(unix_time)
                string = "%s, %d %s %d" % (name, time.day, self.month_list[time.month], time.year)
                self.datastores_label_updated.setText("<b>Updated: </b>" + string)

                string = element['importType'].encode('ascii', 'replace')
                self.datastores_label_import.setText("<b>Import Type: </b>" + string)

                string = element['exportType'].encode('ascii', 'replace')
                self.datastores_label_export.setText("<b>Export Type: </b>" + string)

                string = element['comments'].encode('ascii', 'replace')
                self.datastores_label_comments.setText("<b>Comments: </b>" + string)
        else:
            self.datastores_label_name.setText("<b>Name: </b>")
            self.datastores_label_description.setText("<b>Description: </b>")
            self.datastores_label_created.setText("<b>Created: </b>")
            self.datastores_label_updated.setText("<b>Updated: </b>")
            self.datastores_label_import.setText("<b>Import Type: </b>")
            self.datastores_label_export.setText("<b>Export Type: </b>")
            self.datastores_label_comments.setText("<b>Comments: </b>")


    def onClickRulesItem(self, item, column):
        """
        Evenet listener, called when a QTreeWidgetItem of the Rules QTreeWidget is clicked
        """
        name = item.text(column)
        children = item.childCount()
        if(children == 0 and name != "Recycle Bin"):
            element = self.findListElementByName(self.rules_list, name)

            if(element):
                string = element['name'].encode('ascii', 'replace')
                self.rules_label_name.setText("<b>Name: </b>" + string)

                string = element['description'].encode('ascii', 'replace')
                self.rules_label_description.setText("<b>Description: </b>" + string)

                name = element['createdBy'].encode('ascii', 'replace')
                unix_time = element['created'] * 0.001
                time = datetime.utcfromtimestamp(unix_time)
                string = "%s, %d %s %d" % (name, time.day, self.month_list[time.month], time.year)
                self.rules_label_created.setText("<b>Created: </b>" + string)

                name = element['updatedBy'].encode('ascii', 'replace')
                unix_time = element['updated'] * 0.001
                time = datetime.utcfromtimestamp(unix_time)
                string = "%s, %d %s %d" % (name, time.day, self.month_list[time.month], time.year)
                self.rules_label_updated.setText("<b>Updated: </b>" + string)

                string = element['comments'].encode('ascii', 'replace')
                self.rules_label_comments.setText("<b>Comments: </b>" + string)
        else:
            self.rules_label_name.setText("<b>Name: </b>")
            self.rules_label_description.setText("<b>Description: </b>")
            self.rules_label_created.setText("<b>Created: </b>")
            self.rules_label_updated.setText("<b>Updated: </b>")
            self.rules_label_comments.setText("<b>Comments: </b>")


    def onClickActionsItem(self, item, column):
        """
        Evenet listener, called when a QTreeWidgetItem of the Actions QTreeWidget is clicked
        """
        name = item.text(column)
        children = item.childCount()
        if(children == 0 and name != "Recycle Bin"):
            element = self.findListElementByName(self.actions_list, name)

            if(element):
                string = element['name'].encode('ascii', 'replace')
                self.actions_label_name.setText("<b>Name: </b>" + string)

                string = element['description'].encode('ascii', 'replace')
                self.actions_label_description.setText("<b>Description: </b>" + string)

                name = element['createdBy'].encode('ascii', 'replace')
                unix_time = element['created'] * 0.001
                time = datetime.utcfromtimestamp(unix_time)
                string = "%s, %d %s %d" % (name, time.day, self.month_list[time.month], time.year)
                self.actions_label_created.setText("<b>Created: </b>" + string)

                name = element['updatedBy'].encode('ascii', 'replace')
                unix_time = element['updated'] * 0.001
                time = datetime.utcfromtimestamp(unix_time)
                string = "%s, %d %s %d" % (name, time.day, self.month_list[time.month], time.year)
                self.actions_label_updated.setText("<b>Updated: </b>" + string)

                string = element['comments'].encode('ascii', 'replace')
                self.actions_label_comments.setText("<b>Comments: </b>" + string)
        else:
            self.actions_label_name.setText("<b>Name: </b>")
            self.actions_label_description.setText("<b>Description: </b>")
            self.actions_label_created.setText("<b>Created: </b>")
            self.actions_label_updated.setText("<b>Updated: </b>")
            self.actions_label_comments.setText("<b>Comments: </b>")


    def onClickActionmapsItem(self, item, column):
        """
        Evenet listener, called when a QTreeWidgetItem of the Actions QTreeWidget is clicked
        """
        name = item.text(column)
        children = item.childCount()
        if(children == 0 and name != "Recycle Bin"):
            element = self.findListElementByName(self.actionmaps_list, name)

            if(element):
                string = element['name'].encode('ascii', 'replace')
                self.actionmaps_label_name.setText("<b>Name: </b>" + string)

                string = element['description'].encode('ascii', 'replace')
                self.actionmaps_label_description.setText("<b>Description: </b>" + string)

                name = element['createdBy'].encode('ascii', 'replace')
                unix_time = element['created'] * 0.001
                time = datetime.utcfromtimestamp(unix_time)
                string = "%s, %d %s %d" % (name, time.day, self.month_list[time.month], time.year)
                self.actionmaps_label_created.setText("<b>Created: </b>" + string)

                name = element['updatedBy'].encode('ascii', 'replace')
                unix_time = element['updated'] * 0.001
                time = datetime.utcfromtimestamp(unix_time)
                string = "%s, %d %s %d" % (name, time.day, self.month_list[time.month], time.year)
                self.actionmaps_label_updated.setText("<b>Updated: </b>" + string)

                string = element['comments'].encode('ascii', 'replace')
                self.actionmaps_label_comments.setText("<b>Comments: </b>" + string)
        else:
            self.actionmaps_label_name.setText("<b>Name: </b>")
            self.actionmaps_label_description.setText("<b>Description: </b>")
            self.actionmaps_label_created.setText("<b>Created: </b>")
            self.actionmaps_label_updated.setText("<b>Updated: </b>")
            self.actionmaps_label_comments.setText("<b>Comments: </b>")


    def onClickSessionsItem(self, item, column):
        """
        Evenet listener, called when a QTreeWidgetItem of the Sessions QTreeWidget is clicked
        """
        name = item.text(column)
        children = item.childCount()
        if(children == 0 and name != "Recycle Bin"):
            element = self.findListElementByName(self.sessions_list, name)

            if(element):
                string = element['name'].encode('ascii', 'replace')
                self.sessions_label_name.setText("<b>Name: </b>" + string)

                name = element['createdBy'].encode('ascii', 'replace')
                unix_time = element['created'] * 0.001
                time = datetime.utcfromtimestamp(unix_time)
                string = "%s, %d %s %d" % (name, time.day, self.month_list[time.month], time.year)
                self.sessions_label_created.setText("<b>Created: </b>" + string)

                name = element['updatedBy'].encode('ascii', 'replace')
                unix_time = element['updated'] * 0.001
                time = datetime.utcfromtimestamp(unix_time)
                string = "%s, %d %s %d" % (name, time.day, self.month_list[time.month], time.year)
                self.sessions_label_updated.setText("<b>Updated: </b>" + string)

                string = element['status'].encode('ascii', 'replace')
                self.sessions_label_status.setText("<b>Status: </b>" + string)

                self.sessions_button_open.setEnabled(True)
            else:
                self.sessions_button_open.setEnabled(False)
        else:
            self.sessions_button_open.setEnabled(False)
            self.sessions_label_name.setText("<b>Name: </b>")
            self.sessions_label_created.setText("<b>Created: </b>")
            self.sessions_label_updated.setText("<b>Updated: </b>")
            self.sessions_label_status.setText("<b>Status: </b>")


    def findTreeElementByName(self, tree, name):
        """
        Find an element in a QTreeWidget by name
        If there is no matching element, return None

        /!\ This should be changed to another method, as there can be two element with the same name
        """
        iterator = QtGui.QTreeWidgetItemIterator(tree, QtGui.QTreeWidgetItemIterator.All)
        while iterator.value():
            item = iterator.value()
            if(item.text(0) == name):
                return item
            iterator +=1
        return None


    def findListElementByName(self, element_list, name):
        """
        Find an element in a list of dictionnary, by name
        If there is no matching element, return None

        /!\ This should be changed to another method, as there can be two element with the same name
        """
        for element in element_list:
            if(element.get('name') and element['name'] == name):
                return element
        return False


    def handleRefreshButton(self):
        """
        Event listener triggered by the 'Refresh' button, only update the currently viewed tab
        """
        idx = self.tab_widget.currentIndex()
        menu = ""
        if(idx == 0):
            menu = "datastores"
        elif(idx == 1):
            menu = "rules"
        elif(idx == 2):
            menu = "actions"
        elif(idx == 3):
            menu = "actionmaps"
        elif(idx == 4):
            menu = "sessions"

        if(menu != ""):
            self.refreshData([menu])


    def openSessionDialog(self):
        """
        Event listener triggered by the 'View Session' button
        """
        item = self.sessions_tree.currentItem()
        if(item != None):
            session_path = ""
            while(item != None):
                name = item.text(0)
                session_path = "/" + name + session_path
                item = item.parent()

            self.showSessionDialog(session_path)

    def showSessionDialog(self, session_path):
        if(self.session_dialog != None and self.session_dialog.isVisible()):
            self.session_dialog.initWidget(session_path)

        else:
            self.session_dialog = SessionDialog(self.session, self.url)
            self.session_dialog.initWidget(session_path)
            self.session_dialog.show()

        self.connect(self.session_dialog, QtCore.SIGNAL("request_task_report(PyQt_PyObject)"), self.requestTaskReport)


    def requestTaskReport(self, task_report_data):
        """
        Pass the signal to the parent widget
        """
        self.emit( QtCore.SIGNAL('request_task_report(PyQt_PyObject)'), task_report_data)


class OverviewThread(QtCore.QThread):
    """
    QThread to execute all the requests the the 1Integrate REST API
    """
    def __init__(self,session,url,arg_list):
        """
        init the object with the session, the url, and the list of arguments of the request
        """
        super(OverviewThread, self).__init__()

        self.session = session
        self.url = url
        self.arg_list = arg_list    # this list contain the arguments added to the request url

    def __del__(self):
        self.wait()


    def run(self):
        """
        Default function containing the main logic of the thread
        Function called by OverviewThread.start()
        """
        # keep making request to the API while the full 1Integrate tree has not been searched
        while(len(self.arg_list) > 0):
            arg_list = [self.arg_list[i] for i in range(len(self.arg_list))]    # create a copy to avoid loop problems
            for arg in arg_list:
                self.loadData(arg)


    def loadData(self, arg):
        """
        This function make a request to the API url with specific arguments (arg).
        It analyze the result and search for additionnal request that can be made
        """
        r = self.session.get(self.url % arg)
        if r.status_code == requests.codes.ok:

            # check if the current request is on a root element (sessions, rules, etc.)
            if("/" in arg):
                top_level = False
            else:
                top_level = True

            self.arg_list.remove(arg)   # remove the current url to the list

            json = r.json()
            if(json.get('contents')):   # search inside folder
                json_content = json['contents']

                for content in json_content:
                    temp_arg = arg + '/' + str(content['name'])

                    if(content['type'] == 'folder' and content['name'] != "Recycle Bin"):
                        self.arg_list.append(temp_arg)    # if a folder is found, add it to the list of url to get
                        self.emit( QtCore.SIGNAL('new_node(QString, PyQt_PyObject, PyQt_PyObject)'), temp_arg, top_level, None)

                    else:
                        r = self.session.get(self.url % temp_arg)   # if it's not a folder, make a request for the content of it
                        if r.status_code == requests.codes.ok:
                            json = r.json()
                            self.emit( QtCore.SIGNAL('new_node(QString, PyQt_PyObject, PyQt_PyObject)'), temp_arg, top_level, json)
